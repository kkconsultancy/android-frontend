package com.example.examplebackdropnandbottomsheet.Activities.Student.Admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.examplebackdropnandbottomsheet.MainActivity;
import com.example.examplebackdropnandbottomsheet.ModelClasses.FeedBackChip;
import com.example.examplebackdropnandbottomsheet.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class Add_Event extends AppCompatActivity {
    boolean isuploaded=false;
    MaterialButton start_date,end_date,submit,cancel;
    EditText activity_name,description,tk;
    String activitname,description_about_event;
    ExtendedFloatingActionButton add_label;
    public static final String STORAGE_PATH_UPLOADS = "EVENTS/";
    int day,year,month;
    private Uri filePath;
    String[] events={"Sports","Cultural Activities","Workshops","Oncampus Drives","Offcampus Drives","Seminars"};
    String imageUrl="https://firebasestorage.googleapis.com/v0/b/redants4site.appspot.com/o/EVENTS%2F1580117105221.jpg?alt=media&token=eaa97d49-a5c3-4597-83a1-f110ee87a848";
    private static final int PICK_IMAGE_REQUEST = 234;
    private StorageReference storageReference;
    ImageView imageView;
    Calendar calendar;
    private RecyclerView recyclerVie;
    private View bottomSheet_navigation;
    ExtendedFloatingActionButton edit,check,radio,text,spin;
    int mYear, mMonth, mDay, mHour, mMinute;
    JSONObject jsonObject=new JSONObject();
    TableLayout tableLayout_event;
    Spinner activity_spinner;
    TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
    int count=1,flag=0;
    TableRow tr;
    BottomSheetBehavior behavior;
    String activity_type,startdate,enddate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__event);

        // findViewById
        bottomSheet_navigation = findViewById(R.id.academics_bottomsheet);
        imageView=findViewById(R.id.event_image);
        start_date=findViewById(R.id.start_date);
        end_date=findViewById(R.id.end_date);
        edit=findViewById(R.id.add_edit);
        text=findViewById(R.id.add_text);
        radio=findViewById(R.id.add_radio);
        check=findViewById(R.id.add_check);
        spin=findViewById(R.id.add_spinner);
        activity_spinner=findViewById(R.id.event_spinner);
        add_label=findViewById(R.id.add_labels_for_page);
        submit=findViewById(R.id.event_submit);
        cancel=findViewById(R.id.event_cancel);
        description=findViewById(R.id.description);
        activity_name=findViewById(R.id.activity_name);
        tableLayout_event=findViewById(R.id.addevent_table);

        behavior = BottomSheetBehavior.from(bottomSheet_navigation);




        // Setting Calender and add Label button and image choose
        try{


            calendar = Calendar.getInstance();

            storageReference = FirebaseStorage.getInstance().getReference();

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showFileChooser();
                }
            });

            add_label.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //bottomsheetNavigationTranslations();
                }
            });

        }
        catch (Exception e){

        }

        // selecting start and end dates
        try {

            start_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH);
                    month = calendar.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog datePickerDialog = new DatePickerDialog(Add_Event.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                    start_date.setText(day + "/" + (month + 1) + "/" + year);
//                                    try {
//                                        Date date1 = new SimpleDateFormat("yyyy MM dd'T'HH:mm:ss.SSSZ").parse(start_date.getText().toString());
//                                        startdate=date1.toString();
//                                    }
//                                    catch (Exception e){
//
//                                    }
                                }
                            }, year, month, day);

                    datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                    datePickerDialog.show();
                }
            });

            end_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH);
                    month = calendar.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog datePickerDialog = new DatePickerDialog(Add_Event.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                    end_date.setText(day + "/" + (month + 1) + "/" + year);
//                                    try {
//                                        Date date1 = new SimpleDateFormat("dd MM yyyy 'T'HH:mm:ss Z").parse(end_date.getText().toString());
//                                        enddate=date1.toString();
//                                    }
//                                    catch (Exception e){
//
//                                    }
                                }
                            }, year, month, day);

                    datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                    datePickerDialog.show();
                }
            });
        }
        catch (Exception e){

        }


        // Add Labels for the page
        try{


            tableLayout_event=findViewById(R.id.addevent_table);
            tableLayout_event.setLayoutParams(layoutParams);
            tableLayout_event.setPadding(12,10,12,10);

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    bottomsheetNavigationTranslations(1);


                }
            });
            radio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        bottomsheetNavigationTranslations(2);

                    }
                    catch (Exception e){

                    }
                }
            });

            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                        bottomsheetNavigationTranslations(3);
                    }
                    catch (Exception e){

                    }
                }
            });
            spin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomsheetNavigationTranslations(4);
                }
            });
            check.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomsheetNavigationTranslations(5);
                }
            });
        }
        catch (Exception e){

        }

        // submit event to database
        try{

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    
                    description_about_event=description.getText().toString();
                    activitname=activity_name.getText().toString();
                    
                    if(activitname.equals("")){
                        View toastView = getLayoutInflater().inflate(R.layout.activity_toast, null);

                        // Initiate the Toast instance.

                        TextView m=toastView.findViewById(R.id.toast);
                        String d="Please Provide Event Name";
                        m.setText(d.toUpperCase());
                        Toast toast = new Toast(getApplicationContext());
                        // Set custom view in toast.
                        toast.setView(toastView);
                        //toast.setText(dateStore.getMessage());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        //toast.setGravity(Gravity.BOTTOM, 0,100);
                        toast.show();
                    }
                    else if(description_about_event.equals(""))
                    {
                        View toastView = getLayoutInflater().inflate(R.layout.activity_toast, null);

                        // Initiate the Toast instance.

                        TextView m=toastView.findViewById(R.id.toast);
                        String d="Please Description about Event";
                        m.setText(d.toUpperCase());
                        Toast toast = new Toast(getApplicationContext());
                        // Set custom view in toast.
                        toast.setView(toastView);
                        //toast.setText(dateStore.getMessage());
                        toast.setDuration(Toast.LENGTH_SHORT);
//                        toast.setGravity(Gravity.BOTTOM, 0,100);
                        toast.show();
                    }
                   else if(start_date.getText().toString().equals("Start Date") || end_date.getText().toString().equals("End Date")){


                        View toastView = getLayoutInflater().inflate(R.layout.activity_toast, null);

                        // Initiate the Toast instance.

                        TextView m=toastView.findViewById(R.id.toast);
                        String d="Please Provide Start and End Date";
                        m.setText(d.toUpperCase());
                        Toast toast = new Toast(getApplicationContext());
                        // Set custom view in toast.
                        toast.setView(toastView);
                        //toast.setText(dateStore.getMessage());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        //toast.setGravity(Gravity.BOTTOM, 0,100);
                        toast.show();
                    }
                    else {

                        uploadFile();




                    }

                }
            });


            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

        }catch (Exception e){

        }


        //Recycle view for event selection
        try{

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, events);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            activity_spinner.setAdapter(dataAdapter);
            activity_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    activity_type=parent.getItemAtPosition(position).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        catch (Exception e){

        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private void uploadFile() {

        //checking if file is available
        if (filePath != null) {
            //displaying progress dialog while image is uploading
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading");
            progressDialog.show();

            //getting the storage reference
            StorageReference sRef = storageReference.child(STORAGE_PATH_UPLOADS+ System.currentTimeMillis() + "." + getFileExtension(filePath));

            //adding the file to reference

            sRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //dismissing the progress dialog
                            progressDialog.dismiss();
                            isuploaded=true;
                            try {
                                taskSnapshot.getMetadata().getReference().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        Log.d("URL -------", uri.toString());
                                        imageUrl=uri.toString();

                                        if(imageUrl!=""||imageUrl!=null) { sendJsonToServer();}


                                       // Toast.makeText(getApplicationContext(), uri.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                            catch (Exception e){

                            }




                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            isuploaded=false;
                            Toast.makeText(getApplicationContext(), "Failed to Create Event", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //displaying the upload progress
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        } else {
            //display an error if no file is selected

        }

    }

    public String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public  void sendJsonToServer(){
        try {
            jsonObject.put("activity", activity_type);
            jsonObject.put("start_date", start_date.getText().toString());
            jsonObject.put("end_date", end_date.getText().toString());
            jsonObject.put("description", description_about_event);
            jsonObject.put("event_name",activity_name.getText().toString());
            jsonObject.put("image",imageUrl);

            try{
                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

                String url = "https://us-central1-redants4site.cloudfunctions.net/activities_api_put/activities/";

                StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    public byte[] getBody() throws AuthFailureError {

                        byte[] body = new byte[0];
                        try {
                            body = jsonObject.toString().getBytes("UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            Log.e("Error at ", "Unable to gets bytes from JSON", e.fillInStackTrace());
                        }
                        return body;
                    }


                    @Override
                    public String getBodyContentType() {
                        return "application/json";
                    }
                };


                queue.add(request);
            }
            catch (Exception e){

            }
        }
        catch (Exception e){

        }
    }

    @SuppressLint("ResourceType")
    public void bottomsheetNavigationTranslations(int label_id){

        //pager.setCurrentItem(6);
        tableLayout_event=findViewById(R.id.addevent_table);

        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet_navigation);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);


        switch(label_id){
            case 1:
                addEditText();
                break;
            case 2:
                addRadioButton();
                break;
            case 3:
                addText();
                break;
            case 4:
                addSpinner();
                break;
            case 5:
                addCheckbox();
                break;
        }

    }

    public void addCheckbox(){
        try{
            final TableLayout t1 =(TableLayout) findViewById(R.id.logousertable);
            // Toast.makeText(getApplicationContext(), "Hi", Toast.LENGTH_SHORT).show();
            try {
                t1.removeAllViewsInLayout();
            }
            catch (Exception e){

            }

            TableRow tr1 = new TableRow(getApplicationContext());


            final TextView note = new TextView(getApplicationContext());
            note.setText("Please Provide the options seperated by space");
            note.setGravity(Gravity.CENTER);
            note.setTypeface(Typeface.DEFAULT_BOLD);

            note.setTextColor(getResources().getColor(R.color.orange));
            note.setTextSize(16);
            note.setHighlightColor(getResources().getColor(R.color.orange));
            tr1.addView(note);
            t1.addView(tr1);



            tr = new TableRow(getApplicationContext());
            tk = new EditText(getApplicationContext());
            tk.setHint(" Please provide Check Boxes");

            tk.setTextAppearance(getApplicationContext(), R.style.dynamictextStyle);
            tk.setTextColor(getResources().getColor(R.color.black));


            tr.addView(tk);

            t1.addView(tr,layoutParams);


            TableRow tr2 = new TableRow(getApplicationContext());


            final Button radiobutton = new Button(getApplicationContext());
            radiobutton.setText("Add Check Boxes");
            radiobutton.setGravity(Gravity.CENTER);
            radiobutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        //Toast.makeText(getApplicationContext(), "done", Toast.LENGTH_SHORT).show();
                        String[] radio;
                        String id = tk.getText().toString();
                        if (id != null || id!=" ") {

                            if (id.charAt(id.length() - 1) == ' ') {
                                //Toast.makeText(getApplicationContext(), "TADA", Toast.LENGTH_SHORT).show();
                                radio = tk.getText().subSequence(0, id.length() - 2).toString().split(" ");

                            } else {
                                radio = tk.getText().toString().split(" ");
                            }
                            for (int i=0;i<radio.length;i++){
                                Log.d("Radio",radio[i]);
                            }

                            try{
                                JSONArray jsonArray = new JSONArray(radio);

                                for (int i=0;i<radio.length;i++){
                                    TableRow tr = new TableRow(getApplicationContext());

                                    tr.setLayoutParams(layoutParams);
                                    final CheckBox checkBox=new CheckBox(getApplicationContext());
                                    checkBox.setText(radio[i]);

                                    checkBox.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                           // Log.d("checkbox click",checkBox.getText().toString());
                                        }
                                    });
                                    tr.addView(checkBox);
                                    tableLayout_event.addView(tr, layoutParams);
                                }
                                //tr.addView(checkBox);


                                tr.setPadding(0,0,0,10);
                                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


                                jsonObject.put(count+"checkbox", jsonArray);
                                count+=1;
                            }
                            catch (Exception e){

                            }
                        } else {
                            tk.setError(" Please Provide Radio Buttons");
                        }
                    }
                    catch (Exception e){

                    }
                }
            });
            tr2.addView(radiobutton);
            t1.addView(tr2);
        }
        catch (Exception e){

        }
    }

    public void addEditText(){


        try {



            Iterator<String> iterator=jsonObject.keys();
            while(iterator.hasNext()){
                String key=iterator.next();
                Log.d("keydata",key);
            }


            final TableLayout t1 =(TableLayout) findViewById(R.id.logousertable);
            // Toast.makeText(getApplicationContext(), "Hi", Toast.LENGTH_SHORT).show();
            try {
                t1.removeAllViewsInLayout();
            }
            catch (Exception e){

            }
            tr = new TableRow(getApplicationContext());


            tk = new EditText(getApplicationContext());
            tk.setHint(" Please provide id");
//                tk.setBackgroundResource(R.drawable.edittext_border);

            tk.setTextAppearance(getApplicationContext(), R.style.dynamictextStyle);
            tk.setTextColor(getResources().getColor(R.color.black));


            tr.addView(tk);

            t1.addView(tr,layoutParams);

            TableRow tr1 = new TableRow(getApplicationContext());


            final Button materialButton = new Button(getApplicationContext());
            materialButton.setText("Add Input");
            materialButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id=tk.getText().toString();
                    //Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
                    if(id!=null || !id.contains(" ")) {
                        try {
                            jsonObject.put(count+"edittext" , id);
                            count += 1;

                            Log.d("count", String.valueOf(count));

                            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                            TableRow ty=new TableRow(getApplicationContext());

                            tk = new EditText(getApplicationContext());
                            tk.setHint(" "+id);
                            tk.setBackgroundResource(R.drawable.spinner);


                            tk.setTextAppearance(getApplicationContext(), R.style.dynamictextStyle);



                            ty.addView(tk);
                            ty.setPadding(0,0,0,10);
                            tableLayout_event.addView(ty,layoutParams);


                        } catch (Exception e) {

                        }
                    }
                    else{
                        tk.setError(" Please Provide id");
                    }

                }

            });

            tr1.addView(materialButton);

            t1.addView(tr1, layoutParams);

        }
        catch (Exception e){

        }




 }

    public void addRadioButton(){

        try{
            final TableLayout t1 =(TableLayout) findViewById(R.id.logousertable);
            // Toast.makeText(getApplicationContext(), "Hi", Toast.LENGTH_SHORT).show();
            try {
                t1.removeAllViewsInLayout();
            }
            catch (Exception e){

            }

            TableRow tr1 = new TableRow(getApplicationContext());


            final TextView note = new TextView(getApplicationContext());
            note.setText("Please Provide the options seperated by space");
            note.setGravity(Gravity.CENTER);
            note.setTypeface(Typeface.DEFAULT_BOLD);

            note.setTextColor(getResources().getColor(R.color.orange));
            note.setTextSize(18);
            note.setHighlightColor(getResources().getColor(R.color.orange));
            tr1.addView(note);
            t1.addView(tr1);



            tr = new TableRow(getApplicationContext());
            tk = new EditText(getApplicationContext());
            tk.setHint(" Please provide Radio Buttons");

            tk.setTextAppearance(getApplicationContext(), R.style.dynamictextStyle);
            tk.setTextColor(getResources().getColor(R.color.black));


            tr.addView(tk);

            t1.addView(tr,layoutParams);


            TableRow tr2 = new TableRow(getApplicationContext());


            final Button radiobutton = new Button(getApplicationContext());
            radiobutton.setText("Add Radio Buttons");
            radiobutton.setGravity(Gravity.CENTER);
            radiobutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        String[] radio;
                        String id = tk.getText().toString();
                        if (id != null || id!=" ") {

                            if (id.charAt(id.length() - 1) == ' ') {
                                //Toast.makeText(getApplicationContext(), "TADA", Toast.LENGTH_SHORT).show();
                                radio = tk.getText().subSequence(0, id.length() - 2).toString().split(",");

                            } else {
                                radio = tk.getText().toString().split(" ");
                            }

                            try{
                                JSONArray jsonArray = new JSONArray(radio);


                                TableRow tr = new TableRow(getApplicationContext());

                                tr.setLayoutParams(layoutParams);


                                RadioGroup group = new RadioGroup(getApplicationContext());

                                group.setBackground(getResources().getDrawable(R.drawable.spinner));

                                for (int i = 0; i < radio.length; i++)
                                {
                                    RadioButton rdbtn = new RadioButton(getApplicationContext());
                                    rdbtn.setId(i);
                                    rdbtn.setText(radio[i]);
                                    group.addView(rdbtn);
                                }
                                tr.addView(group);
                                tr.setPadding(0,0,0,10);
                                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                tableLayout_event.addView(tr, layoutParams);

                                jsonObject.put(count+"radiobutton", jsonArray);
                                count+=1;
                            }
                            catch (Exception e){

                            }
                        } else {
                            tk.setError(" Please Provide Radio Buttons");
                        }
                    }
                    catch (Exception e){

                    }
                }
            });
            tr2.addView(radiobutton);
            t1.addView(tr2);
        }
        catch (Exception e){

        }

    }

    public void addText(){
        final TableLayout t1 =(TableLayout) findViewById(R.id.logousertable);
        // Toast.makeText(getApplicationContext(), "Hi", Toast.LENGTH_SHORT).show();
        try {
            t1.removeAllViewsInLayout();
        }
        catch (Exception e){

        }
        tr = new TableRow(getApplicationContext());


        tk = new EditText(getApplicationContext());
        tk.setHint(" Please Enter Text");

        tk.setTextAppearance(getApplicationContext(), R.style.dynamictextStyle);
        tk.setTextColor(getResources().getColor(R.color.black));


        tr.addView(tk);

        t1.addView(tr,layoutParams);


        TableRow tr1 = new TableRow(getApplicationContext());


        final Button materialButton = new Button(getApplicationContext());
        materialButton.setText("Add Input");
        materialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id=tk.getText().toString();
                //Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
                if(id!=null || id!=" ") {
                    try {
                        jsonObject.put(count+"text" , id);
                        count += 1;

                        Log.d("count", String.valueOf(count));

                        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        TableRow ty=new TableRow(getApplicationContext());

                        TextView textView=new TextView(getApplicationContext());
                        textView.setText(id);



                        textView.setTextAppearance(getApplicationContext(), R.style.dynamictextStyle);



                        ty.addView(textView);
                        ty.setPadding(0,0,0,10);
                        tableLayout_event.addView(ty,layoutParams);


                    } catch (Exception e) {

                    }
                }
                else{
                    tk.setError(" Please Enter Text");
                }

            }

        });

        tr1.addView(materialButton);
        t1.addView(tr1);

    }

    public void addSpinner(){
        try{
            final TableLayout t1 =(TableLayout) findViewById(R.id.logousertable);
            // Toast.makeText(getApplicationContext(), "Hi", Toast.LENGTH_SHORT).show();
            try {
                t1.removeAllViewsInLayout();
            }
            catch (Exception e){

            }

            TableRow tr1 = new TableRow(getApplicationContext());


            final TextView note = new TextView(getApplicationContext());
            note.setText("Please Provide the options seperated by space");
            note.setGravity(Gravity.CENTER);
//            note.setTypeface(Typeface.DEFAULT_BOLD);

            note.setTextColor(getResources().getColor(R.color.orange));
            note.setTextSize(16);
            note.setHighlightColor(getResources().getColor(R.color.orange));
            tr1.addView(note);
            t1.addView(tr1);



            tr = new TableRow(getApplicationContext());
            tk = new EditText(getApplicationContext());
            tk.setHint(" Please provide Items");

            tk.setTextAppearance(getApplicationContext(), R.style.dynamictextStyle);
            tk.setTextColor(getResources().getColor(R.color.black));


            tr.addView(tk);

            t1.addView(tr,layoutParams);


            TableRow tr2 = new TableRow(getApplicationContext());


            final Button radiobutton = new Button(getApplicationContext());
            radiobutton.setText("Add Spinner Items");
            radiobutton.setGravity(Gravity.CENTER);
            radiobutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        final String[] radio;
                        String id = tk.getText().toString();
                        if (id != null || id!=" ") {

                            if (id.charAt(id.length() - 1) == ' ') {
                                //Toast.makeText(getApplicationContext(), "TADA", Toast.LENGTH_SHORT).show();
                                radio = tk.getText().subSequence(0, id.length() - 2).toString().split(" ");

                            } else {
                                radio = tk.getText().toString().split(" ");
                            }

                            try{
                                final JSONArray jsonArray = new JSONArray(radio);


                                TableRow tr = new TableRow(getApplicationContext());

                                tr.setLayoutParams(layoutParams);


                                Spinner dynamic_spinner=new Spinner(getApplicationContext());
                                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_spinner_item, radio);
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                dynamic_spinner.setAdapter(dataAdapter);
                                dynamic_spinner.setBackgroundResource(R.drawable.spinner);
                                dynamic_spinner.setMinimumHeight(75);

                                tr.addView(dynamic_spinner);
                                tr.setPadding(0,0,0,10);
                                jsonObject.put(count+"spinner", jsonArray);
                                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                tableLayout_event.addView(tr, layoutParams);


                                count+=1;
                            }
                            catch (Exception e){

                            }
                        } else {
                            tk.setError(" Please Provide Radio Buttons");
                        }
                    }
                    catch (Exception e){

                    }
                }
            });
            tr2.addView(radiobutton);
            t1.addView(tr2);
        }
        catch (Exception e){

        }

    }
}
