package com.example.examplebackdropnandbottomsheet.Activities.Student;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.ArraySet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.examplebackdropnandbottomsheet.Activities.Student.Admin.Add_Event;
import com.example.examplebackdropnandbottomsheet.Activities.Student.Admin.View_Event_Status;
import com.example.examplebackdropnandbottomsheet.ModelClasses.Activity_Model;
import com.example.examplebackdropnandbottomsheet.ModelClasses.Adapters.Activity_Adapter;
import com.example.examplebackdropnandbottomsheet.ModelClasses.Adapters.Nested_Activity_Adapter;
import com.example.examplebackdropnandbottomsheet.R;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.gson.Gson;
import com.nightonke.boommenu.Animation.BoomEnum;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.example.examplebackdropnandbottomsheet.R.drawable.add_black_24dp;
import static com.example.examplebackdropnandbottomsheet.R.drawable.close_black_24dp;

public class Activity_Student extends Fragment {

    List<Activity_Model> activity_students=new ArrayList<>();
    RecyclerView recyclerView;
    String role="admin";
    boolean isOpen = false;
    Context context=getActivity();
    Set<String> hash_Set = new HashSet<String>();
    JSONArray jsonArray ;
    View v;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_activity__student, container, false);


        try{

            if(role=="admin"){


                final ExtendedFloatingActionButton event,addEvent,viewEvent;
                final Animation fab_open,fab_close,fab_rotate_clockwise,fab_rotate_anti_clockwise;



                event=v.findViewById(R.id.event);
                addEvent=v.findViewById(R.id.addEvent);
                viewEvent=v.findViewById(R.id.viewEvent);

                addEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i=new Intent(getContext(), Add_Event.class);
                        startActivity(i);
                    }
                });

                viewEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i=new Intent(getContext(), View_Event_Status.class);
                        startActivity(i);
                    }
                });



                event.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(isOpen) {

                            event.setIconResource(add_black_24dp);
                            addEvent.setVisibility(View.INVISIBLE);
                            viewEvent.setVisibility(View.INVISIBLE);
                            addEvent.setClickable(false);
                            viewEvent.setClickable(false);
                            isOpen =false;


                        }
                        else{

                            event.setIconResource(close_black_24dp);

                            addEvent.setVisibility(View.VISIBLE);
                            viewEvent.setVisibility(View.VISIBLE);
                            addEvent.setClickable(true);
                            viewEvent.setClickable(true);
                            isOpen =true;


                        }
                    }
                });


            }

        }
        catch (Exception e){

        }


        start();


        return v;
    }

    private void start(){
        try {

            try {

                RequestQueue queue = Volley.newRequestQueue(getContext());

                String url = "https://us-central1-redants4site.cloudfunctions.net/activities_api_get/activities/";

// Request a String response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                Log.d("Json 1",response);
                                try {

                                    jsonArray=new JSONArray(response);

                                    try{

                                        for(int n = 0; n < jsonArray.length(); n++) {

                                            hash_Set.add(jsonArray.getJSONObject(n).getString("activity"));

                                        }

                                        Log.d("Hash ", String.valueOf(hash_Set));




                                        for(int n = 0; n < jsonArray.length(); n++)
                                        {

                                            String act=jsonArray.getJSONObject(n).getString("activity");
                                            String image=jsonArray.getJSONObject(n).getString("image");
                                            String start_date=jsonArray.getJSONObject(n).getString("start_date");
                                            String end_date=jsonArray.getJSONObject(n).getString("end_date");
                                            String object=jsonArray.getJSONObject(n).toString();

                                            Activity_Model activity_model=new Activity_Model(act,image,start_date,end_date,object);

                                            activity_students.add(activity_model);

                                        }

                                        if(activity_students.size()>0) {


                                            recyclerView = (RecyclerView) v.findViewById(R.id.activity_recycle);
                                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


                                            Activity_Adapter adapter = new Activity_Adapter(hash_Set,activity_students,new Nested_Activity_Adapter.OnItemClick(){
                                                @Override
                                                public void onItem(Activity_Model item) {
                                                    // Toast.makeText(Eamcet_Colleges.this, "Clicked "+item.getCode(), Toast.LENGTH_SHORT).show();
                                                    Intent r = new Intent(getActivity(),View_Event.class);
//                                                    Gson gson = new Gson();
//                                                    String myJson = gson.toJson(item.getObject());
                                                    r.putExtra("event", item.getObject());
                                                    startActivity(r);
                                                }
                                            });

                                            adapter.notifyDataSetChanged();

                                            recyclerView.setAdapter(adapter);



                                        }
                                    }
                                    catch (Exception e){

                                    }

                                }
                                catch (Exception e){

                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Json ",error.toString());

                    }

                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();


                        return headers;
                    }
                };

// Add the request to the RequestQueue.
                queue.add(stringRequest);
            }
            catch (Exception e){

            }




        }
        catch (Exception e){

        }

    }



    private List<String> getData() {
        List<String> data = new ArrayList<>();
        for (int i = 0; i < BoomEnum.values().length - 1; i++) data.add(BoomEnum.values()[i] + "");
        return data;
    }
}
