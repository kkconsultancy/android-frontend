package com.example.examplebackdropnandbottomsheet.Activities.Student;

import android.os.Bundle;

import com.example.examplebackdropnandbottomsheet.ModelClasses.Activity_Model;
import com.example.examplebackdropnandbottomsheet.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class View_Event extends AppCompatActivity {
    JSONObject result = new JSONObject();
    String  ob;
    RadioButton radio_button;
    TableLayout tableLayout_event;
    Toolbar toolbar;
    JSONObject jsonObject;

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__event);
        try {


            Gson gson = new Gson();
//            ob = gson.fromJson(getIntent().getStringExtra("event"), Activity_Model.class);
            ob=getIntent().getStringExtra("event");

            jsonObject=new JSONObject(ob);
            Log.d("Event ",jsonObject.getString("event_name"));

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle(jsonObject.getString("event_name"));
            setSupportActionBar(toolbar);
            try {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
            catch (Exception e){

            }
            addLables(jsonObject);
        }
        catch (Exception e){

        }


        try{
            MaterialButton materialButton=findViewById(R.id.onEventSubmit);
            materialButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventSubmit();
                }
            });
        }
        catch (Exception e){

        }

    }

    public  void addLables(JSONObject jsonObject){
        try {


            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(10,10,10,10);

            ImageView imageView=findViewById(R.id.event_image);
            Picasso.get().load(jsonObject.getString("image"))
                    .placeholder(R.drawable.person_outline_black_24dp).into(imageView);

            tableLayout_event=new TableLayout(getApplicationContext());
            tableLayout_event=findViewById(R.id.event_table);

            Iterator<String> keys = jsonObject.keys();

            while(keys.hasNext()) {
                final String key = keys.next();
                if ( key.contains("edittext")) {

                    TableRow tr=new TableRow(getApplicationContext());
                    tr.setLayoutParams(lp);
                    final EditText editText=new EditText(new ContextThemeWrapper(this,R.style.TextInputEditTextTheme),null,0);




                    editText.setHint(jsonObject.get(key).toString());

                    editText.setBackground(getResources().getDrawable(R.drawable.spinner));

                    tr.addView(editText);
                    editText.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if (editText.getText().toString() == "") {
                                    editText.setError("Please provide " + editText.getHint());
                                } else {

                                    result.put(editText.getHint().toString(), editText.getText());
                                }

                            }
                            catch (Exception e){

                            }
                        }
                    });

                    tableLayout_event.addView(tr,lp);
                }
                 else if(key.contains("radiobutton")){
                    JSONArray jsonArray=jsonObject.getJSONArray(key);


                     TableRow tr=new TableRow(getApplicationContext());
                     tr.setLayoutParams(lp);


                     RadioGroup group=new RadioGroup(this);


                    for (int i=0;i<jsonArray.length();i++){
                        RadioButton rdbtn = new RadioButton(this);
                        rdbtn.setId( i);
                        rdbtn.setText(jsonArray.get(i).toString());

                        group.addView(rdbtn);
                    }

                    group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            try {
                                RadioButton rb=(RadioButton) findViewById(checkedId);

                                String text=rb.getText().toString();

                                result.put(key,text);


                            }
                            catch (Exception e){

                            }
                        }
                    });
                    tr.addView(group);

                    tableLayout_event.addView(tr,lp);

                }
                 else if(key.contains("spinner")){
                    JSONArray jsonArray=jsonObject.getJSONArray(key);
                    TableRow tr=new TableRow(this);
                    tr.setLayoutParams(lp);

                    Spinner spinner=new Spinner(this);
                    spinner.setBackgroundResource(R.drawable.spinner);

                    spinner.setMinimumHeight(75);
                    final String[] spinnerItems=new String[jsonArray.length()];
                    for (int i=0;i<jsonArray.length();i++){
                        spinnerItems[i]=jsonArray.get(i).toString();
                    }

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                            android.R.layout.simple_spinner_item, spinnerItems);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(dataAdapter);

                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            try{
                                result.put(key,spinnerItems[position]);
                            }
                            catch (Exception e){

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    tr.addView(spinner);


                    tableLayout_event.addView(tr,lp);

                }
                 else if(key.contains("checkbox")){
                    final JSONArray jsonArray=jsonObject.getJSONArray(key);
                    final List<String> checked_items=new ArrayList<String>();
                    final JSONArray j=new JSONArray();




                    for (int i=0;i<jsonArray.length();i++){
                        final CheckBox checkBox=new CheckBox(getApplicationContext());
                        TableRow tr=new TableRow(this);
                        tr.setLayoutParams(lp);
                        //checkBox.setBackgroundResource(R.drawable.spinner);
                        checkBox.setText(jsonArray.get(i).toString());
                        checkBox.setId(i);
                        checkBox.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(checkBox.isChecked()) {
                                    checked_items.add(checkBox.getText().toString());
                                    checkBox.setBackgroundTintList(getColorStateList(R.color.colorPrimaryDark));

                                }
                                else{
                                    checked_items.remove(checkBox.getText().toString());
                                }
                                Log.d("checkbox click",checkBox.getText().toString());

                                j.put(checkBox.getText().toString());
                                try {
                                    result.put("checkbox", j);
                                }
                                catch (Exception e){

                                }
                            }
                        });
                        tr.addView(checkBox);
                        tr.setPadding(0,0,0,10);
                        tableLayout_event.addView(tr, lp);
                    }




                }
                 else if(key.contains("text")){
                    TableRow tr=new TableRow(this);
                    tr.setLayoutParams(lp);

                    TextView textView=new TextView(getApplicationContext());
                    textView.setText(jsonObject.getString(key).toString());



                    textView.setTextAppearance(getApplicationContext(), R.style.dynamictextStyle);



                    tr.addView(textView);
                    tr.setPadding(0,20,0,10);
                    tableLayout_event.addView(tr,lp);
                }
            }

        }
        catch (Exception e){

        }


    }


    public void onEventSubmit(){

        Log.d("\n\n\nResult :::::::::\n\n\n ",result.toString());

        loopQuestions(tableLayout_event);
        Toast.makeText(getApplicationContext(), result.toString(), Toast.LENGTH_SHORT).show();
    }

    private void loopQuestions(ViewGroup parent) {
        JSONArray j=new JSONArray();
        for(int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);

//             if(child instanceof CheckBox) {
//                //Support for Checkboxes
//                CheckBox cb = (CheckBox)child;
//                int answer = cb.isChecked() ? 1 : 0;
//
//            }
             if(child instanceof EditText) {
                //Support for EditText
                EditText et = (EditText)child;
                Log.d("ANDROID DYNAMIC VIEWS:", "EdiText: " + et.getText());

                try {
                    result.put(et.getHint().toString(), et.getText().toString());
                }
                catch (Exception e){

                }
            }
//            else if(child instanceof ToggleButton) {
//                //Support for ToggleButton
//                ToggleButton tb = (ToggleButton)child;
//                Log.w("ANDROID DYNAMIC VIEWS:", "Toggle: " + tb.getText());
//            }
//            else {
//                //Support for other controls
//            }

            if(child instanceof ViewGroup) {
                //Nested Q&A
                ViewGroup group = (ViewGroup)child;
                loopQuestions(group);
            }
        }
    }
}
