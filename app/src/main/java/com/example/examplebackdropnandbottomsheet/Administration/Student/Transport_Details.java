package com.example.examplebackdropnandbottomsheet.Administration.Student;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.examplebackdropnandbottomsheet.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public class Transport_Details extends Fragment {

    View v;
    public Transport_Details() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_transport__details, container, false);

       start();
       return v;
    }
    private void start(){

       try {

           TableLayout t1= v.findViewById(R.id.transport_details_tablelayout);



           JSONObject jsonObject = new JSONObject(readJSONFromAsset());

           Iterator<String> iterator=jsonObject.keys();
           while(iterator.hasNext()){
               String key=iterator.next();
               Log.d("Details",key);
               if (key.contains("regno")) {

                   continue;
               }

               TableRow tr=new TableRow(getContext());
               TextView t=new TextView(getContext());
               tr.setGravity(Gravity.START);
               t.setText(key);
               t.setPadding(5,30,5,5);
               t.setTextAppearance(getContext(),R.style.textStyle);
               t.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
               tr.addView(t);
               t1.addView(tr,new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

               JSONArray jsonArray=jsonObject.getJSONArray(key);

               TableRow tableRow1=new TableRow(getContext());
               Iterator<String> iterator1=jsonArray.getJSONObject(0).keys();
               while (iterator1.hasNext()){
                   String key1=iterator1.next();

                   TextView tv=new TextView(getContext());
                   tv.setTextColor(getResources().getColor(R.color.orange));
                   tv.setPadding(10,10,10,5);
                   tv.setText(key1);
                   tableRow1.addView(tv);

               }
               t1.addView(tableRow1,new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

               for (int i=0;i<jsonArray.length();i++){

                   JSONObject jsonObject1=jsonArray.getJSONObject(i);
                   TableRow tableRow=new TableRow(getContext());
                   Iterator<String> iterator2=jsonObject1.keys();

                   while (iterator2.hasNext()){
                       String key1=iterator2.next();

                       TextView tv=new TextView(getContext());
                       tv.setPadding(10,10,10,5);
                       tv.setText(jsonObject1.getString(key1));
                       tableRow.addView(tv);
                   }

                   t1.addView(tableRow,new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

               }
           }
       }
       catch (Exception e){

       }

    }
    private String readJSONFromAsset() {
        String json = null;
        try {

            InputStream is =getActivity().getAssets().open("fee_details.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
