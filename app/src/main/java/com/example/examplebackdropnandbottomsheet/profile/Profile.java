
package com.example.examplebackdropnandbottomsheet.profile;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;



import com.example.examplebackdropnandbottomsheet.MainActivity;
import com.example.examplebackdropnandbottomsheet.MalbClasses.HttpHandler;
import com.example.examplebackdropnandbottomsheet.MalbClasses.JsonTask;
import com.example.examplebackdropnandbottomsheet.MalbClasses.RetroGet;
import com.example.examplebackdropnandbottomsheet.MalbClasses.RetroServer;
import com.example.examplebackdropnandbottomsheet.R;
import com.google.android.material.card.MaterialCardView;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;

import static android.view.View.TEXT_ALIGNMENT_CENTER;
import static android.view.View.TEXT_ALIGNMENT_GRAVITY;
import static android.widget.LinearLayout.VERTICAL;


public class Profile extends Fragment {

    private View v;

    String key;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

         v= inflater.inflate(R.layout.fragment_profile, container, false);


         try{

             JSONObject jArray = new JSONObject(readJSONFromAsset(v));


             TableLayout tl = (TableLayout) v.findViewById(R.id.personalTable);
             JSONObject personalDetails = jArray.getJSONObject("personal");
             Iterator<String> keys = personalDetails.keys();

             while(keys.hasNext()) {
                 key = keys.next();

                 Log.d("Keys", String.valueOf(key));

                 if (key.equals("Image")) {
                     continue;
                 }
                 TableRow tr = new TableRow(getContext());
                 tr.setPadding(15, 10, 0, 0);

                 TextView tk = new TextView(getContext());
                 tk.setText(key);
                 tr.addView(tk);


                 TextView tv = new TextView(getContext());
                 tv.setText(":   " + personalDetails.getString(key));
                 tr.addView(tv);

                 tl.addView(tr, new LinearLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

             }

                 TableLayout t2 = (TableLayout) v.findViewById(R.id.educationalTable);
                 JSONObject educationalDetails = jArray.getJSONObject("educational");
                 Iterator<String> keys2=educationalDetails.keys();

                 while(keys2.hasNext()){
                     key = keys2.next();

                     if(key.equals("Image")){
                         continue;
                     }

                     TableRow tr = new TableRow(getContext());
                     tr.setPadding(15,10,0,0);

                     TextView tk = new TextView(getContext());
                     tk.setText(key);
                     tr.addView(tk);


                     TextView tv = new TextView(getContext());
                     tv.setText(":   "+educationalDetails.getString(key));
                     tr.addView(tv);

                     t2.addView(tr,new LinearLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

                 }


             TableLayout t3 = (TableLayout) v.findViewById(R.id.othersTable);
             JSONObject othersDetails = jArray.getJSONObject("others");
             Iterator<String> keys3=othersDetails.keys();

             while(keys3.hasNext()){
                 key = keys3.next();

                 if(key.equals("Image")){
                     continue;
                 }

                 TableRow tr = new TableRow(getContext());
                 tr.setPadding(15,10,0,0);

                 TextView tk = new TextView(getContext());
                 tk.setText(key);
                 tr.addView(tk);


                 TextView tv = new TextView(getContext());
                 tv.setText(":   "+othersDetails.getString(key));
                 tr.addView(tv);

                 t3.addView(tr,new LinearLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

             }

             try{


                 CircularImageView imageView=v.findViewById(R.id.logo);
                 Picasso.get().load(educationalDetails.getString("Image"))
                         .placeholder(R.drawable.person_outline_black_24dp).into(imageView);
             }
             catch (Exception e){

             }

         }catch (Exception e){

         }

        return v;

    }


    public String readJSONFromAsset(View v) {
        String json = null;
        try {

            InputStream is =getActivity().getAssets().open("profile.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
