package com.example.examplebackdropnandbottomsheet.Academics.Teacher;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.examplebackdropnandbottomsheet.R;
import com.facebook.shimmer.ShimmerFrameLayout;


public class AttendenceTeacher extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v= inflater.inflate(R.layout.fragment_attendence_teacher, container, false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ShimmerFrameLayout shimmer_view_container =
                        (ShimmerFrameLayout) v.findViewById(R.id.shimmer_view_container);
                shimmer_view_container.setVisibility(View.GONE);
                shimmer_view_container.stopShimmer();
            }
        },10000);


        return v;
    }

}
