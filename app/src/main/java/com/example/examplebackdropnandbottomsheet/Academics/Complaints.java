package com.example.examplebackdropnandbottomsheet.Academics;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.examplebackdropnandbottomsheet.MalbClasses.RetroGet;
import com.example.examplebackdropnandbottomsheet.MalbClasses.RetroServer;
import com.example.examplebackdropnandbottomsheet.ModelClasses.FeedBackChip;
import com.example.examplebackdropnandbottomsheet.R;
import com.google.android.material.chip.Chip;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Complaints extends Fragment {



    RetroGet retroGet;
    List<FeedBackChip> chips;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_complaints, container, false);

        final RecyclerView recyclerVie = (RecyclerView) v.findViewById(R.id.feedbackChipsRecycleView);
        recyclerVie.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));


        retroGet =  RetroServer.getRetrofit("https://api.mongolab.com/api/1/databases/aaa/").create(RetroGet.class);
        Call<List<FeedBackChip>> dataCall = retroGet.getbackChips();

        dataCall.enqueue(new Callback<List<FeedBackChip>>() {
            @Override
            public void onResponse(Call<List<FeedBackChip>> call, Response<List<FeedBackChip>> response) {


                try {
                    chips=response.body();

                    FeedBackChipAdapter feedBackChipAdapter=new FeedBackChipAdapter(chips,R.layout.chips_recycle_view,getContext());

                    recyclerVie.setAdapter(feedBackChipAdapter);
                }
                catch (Exception e){

                }

            }

            @Override
            public void onFailure(Call<List<FeedBackChip>> call, Throwable t) {

            }
        });
        return v;
    }

    public static class FeedBackChipAdapter extends RecyclerView.Adapter<FeedBackChipAdapter.ViewHolder> {
        List<FeedBackChip> list;
        private int rowLayout;
        private Context context;


        public static class ViewHolder extends RecyclerView.ViewHolder {

            Chip chipname;

            public ViewHolder(View v) {
                super(v);

                chipname=v.findViewById(R.id.chipname);
            }
        }

        public FeedBackChipAdapter(List<FeedBackChip> list, int rowLayout, Context context) {
            this.list=list;
            this.rowLayout = rowLayout;
            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent,false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            holder.chipname.setText(list.get(position).getChipname());

        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }


}
