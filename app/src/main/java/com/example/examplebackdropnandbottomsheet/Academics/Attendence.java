package com.example.examplebackdropnandbottomsheet.Academics;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.examplebackdropnandbottomsheet.ModelClasses.AbsentRecords;
import com.example.examplebackdropnandbottomsheet.ModelClasses.DateStore;
import com.example.examplebackdropnandbottomsheet.ModelClasses.Holidays;
import com.example.examplebackdropnandbottomsheet.Others.DayDecorator;
import com.example.examplebackdropnandbottomsheet.R;
import com.google.android.material.card.MaterialCardView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import me.itangqi.waveloadingview.WaveLoadingView;


import static android.view.View.resolveSizeAndState;
import static androidx.constraintlayout.widget.Constraints.TAG;


public class Attendence extends Fragment {

    TextView classattend,classheld,percentage,warning_text;
    private View v;
    private MaterialCalendarView materialCalendarView;
    private WaveLoadingView waveLoadingView;
    private Calendar cal;
    private List<String> monthList=new LinkedList<>();
    private TableLayout tableAttendance,attendance_percentage;
    private static int todayMonth;
    private static int todayYear;
    private static int todayDate;
    private String month;
    private String year;
    private String timestamp;
    private MaterialCardView materialCardView;
    private List<Holidays> holidays=new ArrayList<>();
    private List<AbsentRecords> absentdates = new ArrayList<AbsentRecords>();
    private List<DateStore> dates = new ArrayList<DateStore>();
    private DateStore dateStore=null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

       v = inflater.inflate(R.layout.fragment_attendence, container, false);

       cal=Calendar.getInstance();






       loadAttendancePercentage();

       findViews(v);
       loadCalendar();




        materialCalendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {

                try{
                    marksundays(date.getDate());
                }
                catch (Exception e){

                }
            }
        });

        materialCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                try {

                    String date2 = new SimpleDateFormat("dd/MM/yyyy",Locale.US).format(date.getDate());

                    for (int i=0;i<absentdates.size();i++){
                        if(absentdates.get(i).getDate().equals(date2)){

                             showAbsendPeriodTable(absentdates.get(i).getSubjects(),absentdates.get(i).getDate());
                        }


                    }
                }
                catch (Exception e){

                }

            }
        });


        return  v;
    }

    private void showAbsendPeriodTable(String json,String date){

        materialCardView.setVisibility(View.VISIBLE);
        TableLayout t3 = (TableLayout) v.findViewById(R.id.table_attendence);
        t3.removeAllViews();

        TableRow tableRow1=new TableRow(getContext());
        tableRow1.setGravity(Gravity.CENTER);
        tableRow1.setPadding(15,10,0,0);

        TextView td1 = new TextView(getContext());
        td1.setText(date);
        td1.setTypeface(Typeface.DEFAULT_BOLD);
        td1.setTextColor(getResources().getColor(android.R.color.holo_red_light));
        td1.setGravity(Gravity.RIGHT);
        tableRow1.addView(td1);
        t3.addView(tableRow1,new LinearLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

        try {
            JSONArray jsonArray = new JSONArray(json);


            TableRow tableRow=new TableRow(getContext());
            tableRow.setGravity(Gravity.CENTER);
            tableRow.setPadding(15,10,0,0);

            TextView td = new TextView(getContext());
            td.setText("Subject Name");
            td.setTypeface(Typeface.DEFAULT_BOLD);
            td.setGravity(Gravity.CENTER);
            tableRow.addView(td);

            TextView ts = new TextView(getContext());
            ts.setText("Period Number");
            ts.setTypeface(Typeface.DEFAULT_BOLD);
            ts.setGravity(Gravity.CENTER);
            tableRow.addView(ts);
            t3.addView(tableRow,new LinearLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));


            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject=jsonArray.getJSONObject(i);
                Iterator<String> keys3 = jsonObject.keys();




                while(keys3.hasNext()) {
                    String key = keys3.next();

                    TableRow tr = new TableRow(getContext());
                    tr.setGravity(Gravity.CENTER);
                    tr.setPadding(15,10,0,0);


                    TextView tk = new TextView(getContext());
                    tk.setText(key);
                    tk.setGravity(Gravity.CENTER);
                    tr.addView(tk);

                    TextView tv = new TextView(getContext());
                    tv.setGravity(Gravity.CENTER);
                    tv.setText(":   "+jsonObject.getString(key));
                    tr.addView(tv);




                    t3.addView(tr,new LinearLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

                }
            }

        }
        catch (Exception e){

        }
    }

    private void marksundays(Date date){
        try{
            Log.d("Dates " , date.toString());

            String date1=new SimpleDateFormat("dd/MM/yyyy",Locale.US).format(date);
            String[] date2=date1.split("/");

            for (int i=0;i<date2.length;i++) {
                Log.d("Date2 ",date2[i]);
            }

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR,Integer.parseInt(date2[2]));
            cal.set(Calendar.MONTH, Integer.parseInt(date2[1])-1);
            cal.set(Calendar.DAY_OF_MONTH,  Integer.parseInt(date2[0]));
            int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

            for (int i = 0; i < maxDay; i++) {
                cal.set(Calendar.DAY_OF_MONTH, i + 1);
                if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){

                    materialCalendarView.setSelectedDate(cal.getTime());
                    Log.d("Sundays ",cal.getTime().toString());
                    materialCalendarView.addDecorator(new DayDecorator(v.getContext(), materialCalendarView.getSelectedDate(), -2));

                }

            }
        }
        catch (Exception e){

        }
    }

    private void findViews(View v){
       // waveLoadingView = v.findViewById(R.id.waveLoadingView);
        materialCalendarView=v.findViewById(R.id.material_calendeer_attendence);
        tableAttendance=v.findViewById(R.id.table_attendence);
        materialCardView=v.findViewById(R.id.table_attendence_card);
        attendance_percentage =v.findViewById(R.id.attendance_table);
        classattend=v.findViewById(R.id.classattended);
        classheld=v.findViewById(R.id.classheld);
        percentage=v.findViewById(R.id.percentage_attendance);
        warning_text=v.findViewById(R.id.warning_text);

    }

    private void loadAttendancePercentage(){


        try {
            RequestQueue queue = Volley.newRequestQueue(getContext());

            String url = "http://webprosindia.com/studentapi/api/attendance/rollno?rollno=16K61A0554";
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {


                            try {
                                Log.d("Json 2",response);

                                String object= response.substring(2,response.length()-2).replace("\\\"", "\"");
                                Log.d("Object ",object);


                                JSONObject emp=new JSONObject(object);

                                classattend.setText("Classes Attended\n\n"+(int)Float.parseFloat(emp.getString("Classes Attended")));
                                classheld.setText("Classes Held\n\n"+(int)Float.parseFloat(emp.getString("Classes Held")));
                                percentage.setText(emp.getString("percentage")+"%");

                                try {
                                    int Nclasses;
                                    warning_text=v.findViewById(R.id.warning_text);
                                    int percent=(int)Float.parseFloat(emp.getString("percentage"));
                                    Log.d("percent", String.valueOf(percent));
                                    if ( percent< 65) {

                                        int classA=(int)Float.parseFloat(emp.getString("Classes Attended"));
                                        int classH=(int) Float.parseFloat(emp.getString("Classes Held"));


                                        for(int i=1;i>0;i++){
                                            Log.d("Percent", String.valueOf(percent));
                                            percent=((classA+i)/classH)*100;
                                            if(percent>=65){
                                                Nclasses=i;
                                                if(i==1) {
                                                    warning_text.setText("\nYou Need to Attend " + Nclasses + " period to meet the Attendence Criteria\n");
                                                }
                                                else{
                                                    warning_text.setText("\nYou Need to Attend " + Nclasses + " periods to meet the Attendence Criteria\n");

                                                }
                                                warning_text.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                                                break;
                                            }
                                        }

                                    }
                                    else if(percent<75 && percent>65){
                                        warning_text.setText("\nYour Attendance Level is Ok\n");
                                        warning_text.setTextColor(getResources().getColor(android.R.color.holo_blue_light));
                                    }
                                    else{
                                        warning_text.setText("\nYour Attendance Level is Good\n");
                                        warning_text.setTextColor(getResources().getColor(android.R.color.holo_green_light));
                                    }
                                }
                                catch (Exception e){

                                }

                            }
                            catch (Exception e){

                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Json Attend",error.toString());
                }

            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();

                    headers.put("Authorization", "OAuth " +"c2FTaSR0dWRlbnRAcEkyMDE5RGVjMDV+MDU0MFBNfFdlYnByMCQwNURlYzIwMTl+UE00MDA1fEdlbmVyYXRlZEJ5Q2hpbm5h");
                    return headers;
                }
            };

            queue.add(stringRequest);
        }
        catch (Exception e){

        }
    }

    public String readJSONFromAsset() {
        String json = null;
        try {

            InputStream is =getActivity().getAssets().open("attendence.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void loadCalendar()
    {




        try {
            Date date=new Date();
            marksundays(date);
            JSONArray jArray = new JSONArray(readJSONFromAsset());

            for (int i=0;i<jArray.length();i++){
                JSONObject jsonObject=jArray.getJSONObject(i);

                absentdates.add(new AbsentRecords(jsonObject.getString("date"),jsonObject.get("subjects").toString(),jsonObject.get("rollnumber").toString()));
                Log.d("Dates ",absentdates.get(i).getDate());

                Date lDate =new SimpleDateFormat("dd/MM/yyyy").parse(absentdates.get(i).getDate());
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(lDate);

                materialCalendarView.setSelectedDate(calendar1);
                if(calendar1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || absentdates.contains(calendar1.get(Calendar.DAY_OF_WEEK))){
                    materialCalendarView.addDecorator(new DayDecorator(v.getContext(), materialCalendarView.getSelectedDate(), -2));

                }
                else {
                    materialCalendarView.addDecorator(new DayDecorator(v.getContext(), materialCalendarView.getSelectedDate(), 5));
                }
            }

        }
        catch (Exception e){

        }
    }


























    public void getHolidays(){



                try {
                    holidays.add(new Holidays("01/02/2020","01/03/2020","Nothing"));

                    for (int r=0;r<holidays.size();r++){
                        // Log.d("Message ",holidays.get(r).getHolidayMessage());
                    }

                    if(!holidays.isEmpty()) {
                        for (int h = 0; h < holidays.size(); h++) {


                            String minholydate = holidays.get(h).getFrom();
                            String maxholydate = holidays.get(h).getTo();


                            // if(minholydate.equals(maxholydate)) {
                            try {
                                Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(minholydate);
                                Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(maxholydate);

                                long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
                                long endTime = date2.getTime(); // create your endtime here, possibly using Calendar or Date
                                long curTime = date1.getTime();

                                while (curTime <= endTime) {
                                    Date d = new Date(curTime);
                                    dates.add(new DateStore(holidays.get(h).getHolidayMessage(), d));
                                    Log.d("Message ",d+holidays.get(h).getHolidayMessage());
                                    curTime += interval;
                                }


                            } catch (Exception e) {

                            }

                        }
                    }

                }
                catch (Exception e){

                }

    }

    public void setHolidays(){
        try{

            for (int i = 0; i < dates.size(); i++) {
                Date lDate = (Date) dates.get(i).getDate();
                String ds = new SimpleDateFormat("dd/MM/yyyy").format(lDate);
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(lDate);

                materialCalendarView.setSelectedDate(calendar1);
                materialCalendarView.addDecorator(new DayDecorator(v.getContext(), materialCalendarView.getSelectedDate(), -2));
                System.out.println(" Date is ..." + ds);
            }
        }
        catch (Exception e){

        }
    }
    public void applyColors(String myDate)
    {



        DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH);
        Date mydate;
        int limit=0;
        try {
            String[] wholeDate=myDate.split("/");
            String date="1/"+myDate;
            mydate = dateFormat.parse(date);

            Calendar calendar=Calendar.getInstance();
            calendar.setTime(mydate);
            //int todayYear=Integer.valueOf(todayDate[])
            int wholeMonth=Integer.valueOf(wholeDate[0]);
            int wholeYear=Integer.valueOf(wholeDate[1]);
            if(wholeYear<todayYear)
            {
                //  Log.d(TAG+"todayDate","reaching1");
                limit=calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            }
            else if(wholeYear>todayYear)
            {
                //Log.d(TAG+"todayDate","reaching2");
                limit=-1;
            }else
            {
                if(wholeMonth<todayMonth){
                    //   Log.d(TAG+"todayDate","reaching3");
                    limit=calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                }else if(wholeMonth==todayMonth){
                    // Log.d(TAG+"todayDate","reaching4");
                    limit=todayDate;
                }else
                {         //   Log.d(TAG+"todayDate","reaching5");

                    limit=-1;
                }
            }
            Log.d(TAG+"todayDate",String.valueOf(todayMonth+"-"+todayYear));

            Log.d(TAG+"todayDate",String.valueOf(limit));

            for(int i=1;i<=limit;i++) {

                Date date1;
                Calendar calendar1 = Calendar.getInstance();
                date1 = dateFormat.parse(String.valueOf(i) + "/" + myDate);

                // Log.d(TAG+"dates",String.valueOf(i)+"/"+myDate);
                // Log.d(TAG+"dates",date1.toString());
                calendar1.setTime(date1);
                materialCalendarView.setSelectedDate(calendar1);
                // Log.d(TAG+"sundayCheck",String.valueOf(calendar1.get(Calendar.DAY_OF_WEEK))+" gap "+Calendar.SUNDAY);

                if (calendar1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || dates.contains(calendar1.get(Calendar.DAY_OF_WEEK))) {
                    // Log.d(TAG+"sunday",date1.toString());
                    materialCalendarView.addDecorator(new DayDecorator(v.getContext(), materialCalendarView.getSelectedDate(), -2));
                    Log.d("Sunday : ", String.valueOf(calendar1.get(Calendar.DAY_OF_WEEK)));
                }
                else {
                    materialCalendarView.addDecorator(new DayDecorator(v.getContext(),materialCalendarView.getSelectedDate(), -3));
                    Log.d("Working : ", String.valueOf(calendar1.get(Calendar.DAY_OF_WEEK)));
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
