package com.example.examplebackdropnandbottomsheet.Academics.Student;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.examplebackdropnandbottomsheet.HttpConnectionGetData;
import com.example.examplebackdropnandbottomsheet.ModelClasses.Fragment_Marks_Details;
import com.example.examplebackdropnandbottomsheet.R;
import com.example.examplebackdropnandbottomsheet.ViewPager.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static android.content.Context.MODE_PRIVATE;

public class Fragment_Marks extends Fragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private String s;


    private JSONArray j;
    private   JSONObject job;
    private    List<String> list=new ArrayList<>();
    private List<String> listSecond=new ArrayList<>();
    private   Map<String,JSONObject> map=new LinkedHashMap<>();
    private SeekBar seekBar;
    //private SeekBar seekBarPercentage;
    private    TextView myText,showPercentage,status;
    // private TextView showSemister;
    //private TextView individualStatus;
    private    int maxX;
    private int imageId;
    private ProgressBar progressBar;
    private LinearLayout linearLayout;
    private View v;
    private String MARKS="MARKS";
    private String TAG="Fragment_Marks_Log";
    private String statusExcellent="Excellent";
    private String statusAverage="Average";
    private String statusInsufficient="Insufficient";
    private String finalPercentage="0.0";
    private int numberofsems;
    private DecimalFormat df;
    private String  UNAME;
    private Map<String,String> myPercentage=new LinkedHashMap<>();

    public void stopTouchListener(SeekBar myseekBar)
    {
        myseekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
    v=inflater.inflate(R.layout.fragment_fragment__marks, container, false);
    seekBar=v.findViewById(R.id.seekBar);
    myText=v.findViewById(R.id.myView);

    SharedPreferences sharedPreferences=v.getContext().getSharedPreferences("MyLogin",MODE_PRIVATE);
    UNAME=sharedPreferences.getString("username","");
        df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.FLOOR);
    progressBar=v.findViewById(R.id.marksProgress);
    linearLayout=v.findViewById(R.id.marksLayout);
    status=v.findViewById(R.id.status);
    // individualStatus=v.findViewById(R.id.individualStatus);
    stopTouchListener(seekBar);
    //  stopTouchListener(seekBarPercentage);
    Point point=new Point();
        try {
        getActivity().getWindowManager().getDefaultDisplay().getSize(point);
    }
        catch (Exception e){

    }

    maxX=point.x;
    showPercentage();
        return v;

}
    public void showStatus(final SeekBar seekBar, final TextView myText)
    {
        final ViewTreeObserver viewTreeObserver = seekBar.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int val = (seekBar.getProgress() * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                    int textViewX = val - (myText.getWidth() / 2);
                    int finalX = myText.getWidth() + textViewX > maxX ? (maxX - myText.getWidth()) : textViewX /*your margin*/;
                    myText.setX(finalX < 0 ? 0/*your margin*/ : finalX);
                    seekBar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
    }
    public void setUpImages(Integer ff,TextView textView)
    {
        if (ff <= 10) {
            if (ff >= 7) {
                imageId = R.drawable.high_logo;
                textView.setText(statusExcellent);

            } else if (ff >= 6) {
                imageId = R.drawable.medium_logo;
                textView.setText(statusAverage);

            } else {
                textView.setText(statusInsufficient);
                imageId = R.drawable.low_logo;
            }
        } else {
            if (ff >= 70) {
                imageId = R.drawable.high_logo;
                textView.setText(statusExcellent);
            } else if (ff >= 60) {
                imageId = R.drawable.medium_logo;
                textView.setText(statusAverage);

            } else {
                imageId = R.drawable.low_logo;
                status.setText(statusInsufficient);

            }

        }
    }
    public void showPercentage()
    {
        showProgress();
        int count=0;
        // tabLayout.removeAllViewsInLayout();
        String q="{\"regno\":{$eq:\""+ UNAME+"\"}}";

        String response;//= readJSONFromAsset() ;


        RequestQueue queue = Volley.newRequestQueue(getContext());

        String url = "http://webprosindia.com/studentapi/api/externalmarks/rollno?rollno=16k61a0554";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {




                            if(response!=null)
                            {
                                //  Log.d(TAG,response.body());
                                String json= response.substring(1,response.length()-1).replace("\\\"", "\"");

                                Log.d("Json 2",json);
                                try {
                                    j = new JSONArray(json);
                                    job = j.getJSONObject(0);
                                    Iterator<String> it1 = job.keys();
                                    Iterator<String> it = job.keys();
                                    Iterator<String> finalpercent=job.keys();
                                    while (it.hasNext()) {
                                        s = it.next();
                                        try{
                                            // Log.d(TAG,s);
                                            //  Log.d(TAG,job.getJSONObject(s).toString());
                                            myPercentage.put(s, job.getJSONObject(s).getJSONObject("final").getString("%"));
                                        }catch (JSONException e)
                                        {

                                        }
                                    }


                                    try {


                                        Log.d("percentage should come", String.valueOf(63.8));

                                        while(finalpercent.hasNext()){

                                            String key=finalpercent.next();
                                            Log.d("object",key);
                                            try {


                                                JSONObject g=job.getJSONObject(key).getJSONObject("final");

                                                Log.d("percentage total",g.toString());
                                                Iterator<String> u=g.keys();

                                                while(u.hasNext()){
                                                    String percentage1=u.next();
                                                    Log.d("percentage Key",percentage1);

                                                    if(percentage1.contains("SGPA")){

                                                        Log.d("value",job.getJSONObject(key).getJSONObject("final").getString(percentage1).toString());
                                                        finalPercentage=String.valueOf(Double.valueOf(finalPercentage)+Double.valueOf(job.getJSONObject(key).getJSONObject("final").getString(percentage1)));
                                                        numberofsems+=1;
                                                        break;
                                                    }

                                                }




                                            }
                                            catch (Exception e){

                                            }
                                        }

                                        Log.d("final",finalPercentage.toString());

                                    }
                                    catch (Exception e){

                                    }





                                    try {
                                        df = new DecimalFormat("#.##");
                                        df.setRoundingMode(RoundingMode.FLOOR);

                                        finalPercentage = df.format(Double.valueOf(finalPercentage));
                                        Log.d("finalPercentage",finalPercentage);



                                        Log.d("value", String.valueOf(numberofsems));

                                        Double value=Double.parseDouble(finalPercentage)/numberofsems*1.0;

                                        //TODO:PASTE HERE
                                        seekBar.setMax(10);
                                        setUpImages(value.intValue(),status);
                                        myText.setText(value.toString().substring(0,4));
                                        seekBar.setProgress(value.intValue());
                                        showStatus(seekBar,myText);
                                        showStatus(seekBar,status);





                                    } catch (Exception e) {
                                        //  Log.d(TAG,String.valueOf(e));
                                    }
                                    try{

                                        seekBar.setThumb(getResources().getDrawable(imageId));
                                    }catch (Exception e)
                                    {
                                        e.printStackTrace();
                                    }
                                    //  seekBar.setThumb(drawForSeekBar(v.getContext(),R.drawable.thumb_image_ic_cool));
                                    while (it1.hasNext()) {
                                        String data=it1.next();

                                        if(data.equals("regno"))
                                        {
                                            continue;
                                        }else
                                            list.add(data);
                                    }
                                    for (String s : list) {
                                        try {
                                            map.put(s, job.getJSONObject(s));
                                        } catch (JSONException e) {
                                            //  Log.d(TAG+"Exception here",e.getMessage());
                                            e.printStackTrace();
                                        }
                                    }
                                    map.remove("_id");
                                    for (Map.Entry<String, JSONObject> maps : map.entrySet()) {
                                        listSecond.add(maps.getValue().toString());
                                    }

                                    viewPager= v.findViewById(R.id.viewPager);
                                    tabLayout=v.findViewById(R.id.tabs);
                                    viewPager.setOffscreenPageLimit(8);
                                    ViewPagerAdapter adapter=new ViewPagerAdapter(getChildFragmentManager(),map,listSecond);
                                    viewPager.setAdapter(adapter);
                                    // viewPager.arrowScroll(ViewPager.FOCUS_RIGHT);
                                    tabLayout.setupWithViewPager(viewPager);
                                    // progressBar.setVisibility(View.INVISIBLE);
                                    // relativeLayout.setVisibility(View.VISIBLE);
                                    tabListener();
                                    hideProgress();


                                } catch (Exception e) {
                                    hideProgress();
                                    e.printStackTrace();
                                }


                            }
                            else{
                                hideProgress();
                                //progressBar.setVisibility(View.INVISIBLE);

                            }


                        }
                        catch (Exception e){




                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Json Marks",error.toString());

            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();

                headers.put("Authorization", "OAuth " +"c2FTaSR0dWRlbnRAcEkyMDE5RGVjMDV+MDU0MFBNfFdlYnByMCQwNURlYzIwMTl+UE00MDA1fEdlbmVyYXRlZEJ5Q2hpbm5h");
                return headers;
            }
        };

        queue.add(stringRequest);








    }
    public void tabCall(String myTab)
    {
        Double per=Double.valueOf(myPercentage.get(myTab));
        String percentage=df.format(per);
        Double original=Double.parseDouble(percentage);
        showPercentage.setText(percentage);
        // seekBarPercentage.setProgress(original.intValue());
        // showStatus(seekBarPercentage,showPercentage);
        // showStatus(seekBarPercentage,individualStatus);
        // setUpImages(original.intValue(),individualStatus);
        try{
            // seekBarPercentage.setThumb(getResources().getDrawable(imageId));
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void tabMethod(TabLayout.Tab tab)
    {
        try{
            String getTab=tab.getText().toString();
            String myTab= getTab.toLowerCase().replace(" ", "");
            // tabCall(myTab);
            // showSemister.setText(getTab);
        }catch (Exception E)
        {

        }
    }
    public  void tabListener()
    {
        // tabCall("sem11");
        // showSemister.setText("SEM 11");
        TabLayout.Tab tab=new TabLayout.Tab();
        // tabMethod(tab);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                try{
                    String getTab=tab.getText().toString();
                    String myTab= getTab.toLowerCase().replace(" ", "");
                    // tabCall(myTab);
                    //  showSemister.setText(getTab);
                }catch (Exception e)
                {
                    Log.d(TAG+"seekException",e.getMessage());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    public void showProgress()
    {
        progressBar.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.INVISIBLE);
    }
    public void hideProgress()
    {
        progressBar.setVisibility(View.INVISIBLE);
        linearLayout.setVisibility(View.VISIBLE);
    }
    public String readJSONFromAsset() {
        String json = null;
        try {

            InputStream is =getActivity().getAssets().open("marks.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}