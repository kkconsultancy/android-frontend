package com.example.examplebackdropnandbottomsheet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;
import com.example.examplebackdropnandbottomsheet.ModelClasses.GlideApp;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.JsonObject;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static android.view.View.GONE;
import static android.view.View.TEXT_ALIGNMENT_CENTER;

public class LogoActivity extends AppCompatActivity {

    private StorageReference mStorageRef;
    FrameLayout framelayout;
    boolean t;
    EditText number,otp;
    MaterialButton requestOTP,submitOTP;
    ImageView logo;
    TextView changenumber;
    FirebaseAuth mAuth;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    HttpConnectionGetData httpConnectionGetData;
    LinearLayout requestotpLayout,submitotpLayout,unabletoLoginLayout;
    SharedPreferences.Editor editor;
    BottomSheetBehavior behavior;
    JSONArray jsonArray;
    SharedPreferences sharedpreferences;
    String phonenumber,verificationCode,token;
    PhoneAuthProvider.ForceResendingToken resendToken;
    Set<String> user_Set;

    int verifyAuth=1;

    String userExists="https://us-central1-site-org-automation.cloudfunctions.net/api/v1/auth/getUid";
    String urlIsUserAvailable="https://us-central1-site-org-automation.cloudfunctions.net/api/v1/isPhoneNumberAvailable";
    String method="POST";
    String userUrl="https://us-central1-site-org-automation.cloudfunctions.net/api/v1/auth/getUser";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);

        try {
            sharedpreferences = getSharedPreferences("User", MODE_PRIVATE);
            editor = sharedpreferences.edit();
            editor.putString("uid", "");
            editor.apply();

        }
        catch (Exception e){

        }


        try{
            try {
               changenumber = findViewById(R.id.changenumber);
                changenumber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mAuth.signOut();

                        user_Set=new HashSet<>();
                        user_Set.clear();

                        editor=sharedpreferences.edit();
                        editor.putStringSet("users",null);
                        editor.apply();

                        afterNetworkChecking();

                    }
                });

            }
            catch (Exception e){

            }
        }
        catch (Exception e){

        }

        try {
            getSupportActionBar().hide();

            mAuth = FirebaseAuth.getInstance();
            mStorageRef = FirebaseStorage.getInstance().getReference();

        } catch (Exception e) {

        }

        try {
            requestotpLayout = findViewById(R.id.requestotplayout);
            submitotpLayout = findViewById(R.id.otplayout);
            unabletoLoginLayout = findViewById(R.id.unableloginlayout);

            number = findViewById(R.id.phone);
            otp = findViewById(R.id.otp);
            submitOTP = findViewById(R.id.otpSubmitButton);
            requestOTP = findViewById(R.id.requestOtpbutton);


            requestotpLayout.setVisibility(GONE);
            submitotpLayout.setVisibility(GONE);
            unabletoLoginLayout.setVisibility(GONE);

        } catch (Exception e) {

        }
        try {



            new Handler().postDelayed(new Runnable() {


                @Override
                public void run() {

                    try {

                        if(sharedpreferences.getString("verifiedUser", "").equals("1")){
                            try {
                                requestotpLayout.setVisibility(GONE);
                                unabletoLoginLayout.setVisibility(GONE);
                                submitotpLayout.setVisibility(View.VISIBLE);
                                user_Set = new HashSet<>();
                                user_Set = sharedpreferences.getStringSet("users", null);


                                otp.setVisibility(GONE);

                                submitOTP.setText("USERS");
                                submitOTP.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        getUserData();
                                        bottomsheetNavigationTranslations(user_Set);
                                    }
                                });
                            } catch (Exception e) {
                            }
                        }
                        else{
                            try {

                                requestotpLayout.setVisibility(View.VISIBLE);
                                submitotpLayout.setVisibility(GONE);
                                unabletoLoginLayout.setVisibility(View.VISIBLE);

                                snackBar();


                            } catch (Exception e) {

                            }
                        }




                        if (mAuth.getCurrentUser() != null) {
                            try {
                                requestotpLayout.setVisibility(GONE);
                                unabletoLoginLayout.setVisibility(GONE);
                                submitotpLayout.setVisibility(View.VISIBLE);
                                user_Set = new HashSet<>();
                                user_Set = sharedpreferences.getStringSet("users", null);


                                otp.setVisibility(GONE);

                                submitOTP.setText("USERS");
                                submitOTP.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        getUserData();
                                        bottomsheetNavigationTranslations(user_Set);
                                    }
                                });
                            } catch (Exception e) {
                            }
                        } else {
                            try {

                                requestotpLayout.setVisibility(View.VISIBLE);
                                submitotpLayout.setVisibility(GONE);
                                unabletoLoginLayout.setVisibility(View.VISIBLE);

                                snackBar();


                            } catch (Exception e) {

                            }


                        }
                    }
                    catch (Exception e){

                    }
                }
            }, 2000);




        } catch (Exception e) {

        }




    }
    void snackBar(){

        t=isNetworkAvaliable(getApplicationContext());

        if(t) {

            afterNetworkChecking();
        }
        else{
            final Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.logoframelayout), "No Network", Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            t=isNetworkAvaliable(getApplicationContext());
                            if(t) {
                                afterNetworkChecking();
                            }
                            else{
                                snackBar();
                            }
                        }
                    });
            View snackbarView = snackbar.getView();
            snackbarView.setMinimumHeight(25);
            snackbarView.setBackgroundResource(R.color.white);
            TextView textView = snackbarView.findViewById(R.id.snackbar_action);
            textView.setTextColor(Color.BLACK);
            TextView textView1=snackbarView.findViewById(R.id.snackbar_text);
            textView1.setTextSize(25);
            textView.setTextSize(15);
            snackbar.show();
        }
    }

    void afterNetworkChecking(){

        requestotpLayout.setVisibility(View.VISIBLE);
        submitotpLayout.setVisibility(GONE);
        unabletoLoginLayout.setVisibility(View.VISIBLE);

        if (t) {

            TextView link_signup=findViewById(R.id.link_signup);
            link_signup.setText("Unable to Login ! \nContact Your Administrator");

            try {
                number = findViewById(R.id.phone);
                otp = findViewById(R.id.otp);


                requestOTP = findViewById(R.id.requestOtpbutton);
                requestOTP.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {



                        phonenumber = "+91" + number.getText().toString();



                        if (phonenumber == "" || phonenumber == " " || phonenumber.length() < 10) {

                            number.setError("Enter Valid Number");

                        } else {

                            String query = "{\"phoneNumber\":\"" + phonenumber + "\"}";
                            httpConnectionGetData = new HttpConnectionGetData(LogoActivity.this, urlIsUserAvailable, method, query, 1);
                            httpConnectionGetData.execute();

                        }


                    }
                });


                submitOTP = findViewById(R.id.otpSubmitButton);
                submitOTP.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {



                        if (otp.getText().toString() == "" || otp.getText().toString() == " ") {
                            otp.setError("Enter Valid OTP");
                        } else {
                            verifyOYP();

                        }

                    }
                });

            } catch (Exception e) {

            }

            try {
                logo = findViewById(R.id.mainlogo);
                //Picasso.get().load("https://firebasestorage.googleapis.com/v0/b/site-org-automation.appspot.com/o/APPLICATION%2FV1%2FlogoAAA.png?alt=media&token=744941f2-a7ff-41c0-868e-6d0c4fee19a7").resize(200,200).placeholder(R.drawable.sasilogo).into(logo);
                GlideApp.with(getApplicationContext()).asBitmap().load("https://firebasestorage.googleapis.com/v0/b/site-org-automation.appspot.com/o/APPLICATION%2FV1%2Flogo.png?alt=media&token=b3b4b2ef-2ae3-4464-b5c0-6e9ba098d948").override(100).apply(new RequestOptions().transform(new RoundedCorners(0))).signature(new ObjectKey(System.currentTimeMillis())).listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        //imageProgress.setVisibility(View.INVISIBLE);
                        Log.d("glideLoad", e.getMessage());
                        logo.setImageDrawable(getResources().getDrawable(R.drawable.logo));

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        logo.setImageBitmap(resource);

                        return false;
                    }
                }).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).submit();
            } catch (Exception e) {

            }
        }
        else{

        }
    }

    void bottomsheetNavigationTranslations(Set<String> jsonArray){
        try {

            TableLayout tl =null ;
            tl=(TableLayout) findViewById(R.id.logousertable);

            tl.removeAllViews();

            View bottomSheet_navigation = findViewById(R.id.academics_bottomsheet);
            behavior = BottomSheetBehavior.from(bottomSheet_navigation);
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);


            for(String stock:jsonArray){
                Log.d("HASH VALUE", (String) stock);
                TableRow tr = new TableRow(getApplicationContext());


                final TextView tk = new TextView(getApplicationContext());


                tk.setText(stock.toUpperCase());


                tk.setTypeface(null, Typeface.BOLD);
                tk.setWidth(250);
                tk.setHeight(100);
                tk.setTextAppearance(this, R.style.dynamictextStyle);
                tk.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                tk.setGravity(Gravity.CENTER);
                tk.setTextColor(getResources().getColor(R.color.colorPrimary));

                tk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), tk.getText().toString(), Toast.LENGTH_SHORT).show();
                        String query= "{ \"uid\":\""+tk.getText().toString().toLowerCase()+"\" }";
                        httpConnectionGetData=new HttpConnectionGetData(LogoActivity.this,userUrl,method,query,3);
                        httpConnectionGetData.execute();

                        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                        Intent i=new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(i);

                    }
                });

                tk.setPadding(0, 0, 0, 0);
                tr.addView(tk);

                tl.addView(tr, new TableLayout.LayoutParams(
                        TableLayout.LayoutParams.WRAP_CONTENT,
                        TableLayout.LayoutParams.WRAP_CONTENT));
            }





            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomsheet, int newState) {

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    // React to dragging events


                }
            });
        }catch (Exception e){

        }
    }

    public  void getUserData(){

        Log.d("UID",mAuth.getCurrentUser().getUid());

        mAuth.getCurrentUser().getIdToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
            @Override
            public void onSuccess(GetTokenResult result) {
                token = result.getToken();
                Log.d("Token", "Get Current Token"+ token.toString());
                if(token!=null){
                    try {

                        JsonObject person = new JsonObject();
                        person.addProperty("type", "phoneNumber");
                        person.addProperty("data", "+919963912971");
                        person.addProperty("uid", mAuth.getCurrentUser().getUid());
                        person.addProperty("token", token);



                        if(person.toString().length()>0) {

                            httpConnectionGetData = new HttpConnectionGetData(LogoActivity.this, "https://us-central1-site-org-automation.cloudfunctions.net/api/v1/auth/getUid", method, person.toString(), 2);

                            httpConnectionGetData.execute();
                        }
                    }
                    catch (Exception e){

                    }
                }

            }
        });

    }


    void sendOTP(String phoneNumber){

        try {

            requestotpLayout.setVisibility(GONE);
            submitotpLayout.setVisibility(View.VISIBLE);
            unabletoLoginLayout.setVisibility(View.VISIBLE);


            mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                @Override
                public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                    Toast.makeText(getApplicationContext(),"Verification Completed",Toast.LENGTH_SHORT).show();


                    submitotpLayout.setVisibility(View.VISIBLE);
                    requestotpLayout.setVisibility(GONE);
                    unabletoLoginLayout.setVisibility(GONE);

                    otp.setVisibility(GONE);
                    submitOTP.setVisibility(View.VISIBLE);

                    submitOTP.setText("USERS");
                    submitOTP.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            bottomsheetNavigationTranslations(user_Set);
                        }
                    });

                    signIN(phoneAuthCredential);

                }

                @Override
                public void onVerificationFailed(FirebaseException e) {
                    Toast.makeText(getApplicationContext(),"Verification Fialed",Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                    super.onCodeSent(s, forceResendingToken);
                    verificationCode = s;

                    resendToken=forceResendingToken;
                    Log.d("Verification Code",verificationCode);

//                    requestotpLayout.setVisibility(GONE);
//                    submitotpLayout.setVisibility(View.VISIBLE);
//                    unabletoLoginLayout.setVisibility(View.VISIBLE);
//
//                    otp.setVisibility(View.VISIBLE);
//                    submitOTP.setVisibility(View.VISIBLE);


                    Toast.makeText(getApplicationContext(),"Verification Code Send",Toast.LENGTH_SHORT).show();
                }
            };

            PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, 60, TimeUnit.SECONDS, LogoActivity.this, mCallback,resendToken);
        }
        catch (Exception e){

        }
    }

    void signIN(final PhoneAuthCredential credential){
        try {
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {

                                requestotpLayout.setVisibility(GONE);
                                submitotpLayout.setVisibility(View.VISIBLE);
                                otp.setVisibility(GONE);
                                unabletoLoginLayout.setVisibility(GONE);
                                submitOTP.setVisibility(View.VISIBLE);


                                getUserData();


                                submitOTP.setText("USERS");
                                submitOTP.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // user_Set=sharedpreferences.getStringSet("users",null);
                                        bottomsheetNavigationTranslations(user_Set);
                                    }
                                });
                                Log.d("Credentials", String.valueOf(credential));




                            } else {
                                Toast.makeText(getApplicationContext(), "Incorrect OTP", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
        catch (Exception e){

        }
    }

    void verifyOYP(){
        try {

            String otpmessage = otp.getText().toString();

            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCode, otpmessage);

            signIN(credential);

        }
        catch (Exception e){

        }
    }

    public void callback(int key){

        try{
            switch(key){
                case 1:
                    try{
                        Log.d("Response", httpConnectionGetData.getResponse());

                        JSONObject jsonObject=new JSONObject(httpConnectionGetData.getResponse());
                        userExists=jsonObject.getString("exists");


                        if(userExists=="" || userExists=="false"){
                            Toast.makeText(getApplicationContext(), "User Does Not Exists", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            sendOTP(phonenumber);

                        }
                    }
                    catch (Exception e){

                    }

                    break;
                case 2:
                    try{
                        Log.d("Response", httpConnectionGetData.getResponse());
                        JSONObject jsonObject=new JSONObject(httpConnectionGetData.getResponse());
                        jsonArray=jsonObject.getJSONArray("uidList");
                        Log.d("Length  ", String.valueOf(jsonArray.length()));

                        if(jsonArray.length()>0) {
                            editor = sharedpreferences.edit();
                            user_Set = new HashSet<String>();
                            for (int i=0;i<jsonArray.length();i++) {
                                user_Set.add(jsonArray.getString(i));
                            }
                            editor.putStringSet("users",user_Set);
                            editor.putString("verifiedUser","1");
                            editor.apply();
                            user_Set=sharedpreferences.getStringSet("users",null);
                            bottomsheetNavigationTranslations(user_Set);
                        }


                    }
                    catch (Exception e){

                    }
                    break;
                case 3:
                    try{
                        Log.d("Response", httpConnectionGetData.getResponse());
                        JSONObject jsonObject=new JSONObject(httpConnectionGetData.getResponse());
                        String rollnumber=jsonObject.getString("uid");
                        Log.d("Roll Number  ",rollnumber);

                        editor = sharedpreferences.edit();
                        editor.putString("uid", rollnumber);
                        editor.apply();

                        Intent i=new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(i);
                        finish();

                    }
                    catch (Exception e){

                    }
                    break;

            }
        }
        catch (Exception e){

        }
    }

    public static boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED)) {
            return true;
        } else {
            return false;
        }
    }

//    public void profile()
//    {
//
//        StorageReference childRef=mStorageRef.child("USERS/"+uid+".JPG");
//        Log.d("glideLoad",childRef.toString());
//        GlideApp.with(getApplicationContext()).asBitmap().load(childRef).override(100).apply(new RequestOptions().transform(new RoundedCorners(40))).signature(new ObjectKey(System.currentTimeMillis())).listener(new RequestListener<Bitmap>() {
//            @Override
//            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
//                //imageProgress.setVisibility(View.INVISIBLE);
//                Log.d("glideLoad",e.getMessage());
//                profile_photo.setImageDrawable(getResources().getDrawable(R.drawable.mydefault));
//
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
//                profile_photo.setImageBitmap(resource);
//
//                return false;
//            }
//        }).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).submit();
//    }

}