package com.example.examplebackdropnandbottomsheet.ModelClasses.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.examplebackdropnandbottomsheet.Activities.Student.View_Event;
import com.example.examplebackdropnandbottomsheet.ModelClasses.Activity_Model;
import com.example.examplebackdropnandbottomsheet.ModelClasses.Colleges;
import com.example.examplebackdropnandbottomsheet.R;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Activity_Adapter extends RecyclerView.Adapter<Activity_Adapter.MyViewHolder> {

    Context context;
    private Set<String> modelList;
    private List<Activity_Model> imageList1=new ArrayList<>();
    private final Nested_Activity_Adapter.OnItemClick click;
    private RecyclerView.RecycledViewPool viewPool;

    private LinearLayoutManager horizontalManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView activity_name;

        public RecyclerView nested_recycleview;
        public MyViewHolder(View view) {
            super(view);
            activity_name =  view.findViewById(R.id.activity_title);
            nested_recycleview=view.findViewById(R.id.nested_activity_recycleview);

        }

        public void bind(final Activity_Model l,final OnItemClick vv){
            activity_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    vv.onItem(l);
                    //Intent i =new Intent(context, View_Event.class);

                }
            });
        }
    }


    public Activity_Adapter(Set<String> moviesList, List<Activity_Model> list1, Nested_Activity_Adapter.OnItemClick click) {
        this.click=click;
        this.modelList = moviesList;
        this.imageList1=list1;

        viewPool=new RecyclerView.RecycledViewPool();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_nested_recycleview, parent, false);


        return new MyViewHolder(itemView);
    }
    public  interface OnItemClick{
        void onItem(Activity_Model item);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        List<String> stringsList = new ArrayList<>(modelList);
        List<Activity_Model> imageList2=new ArrayList<>();

        holder.activity_name.setText(stringsList.get(position));


        Log.d("Event ",stringsList.get(position));


        for (Activity_Model customer : imageList1) {
            if (customer.getActivity().equals(stringsList.get(position))) {
                imageList2.add(customer);
            }
        }



        holder.nested_recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));

        Nested_Activity_Adapter nested_activity_adapter=new Nested_Activity_Adapter(imageList2, click);
        holder.nested_recycleview.setAdapter(nested_activity_adapter);
        holder.nested_recycleview.setRecycledViewPool(viewPool);
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }
}
