package com.example.examplebackdropnandbottomsheet.ModelClasses;

import java.util.Date;

public class DateStore {

    private String message;

    private Date date;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public DateStore(String message, Date date) {
        this.message = message;
        this.date = date;
    }
}
