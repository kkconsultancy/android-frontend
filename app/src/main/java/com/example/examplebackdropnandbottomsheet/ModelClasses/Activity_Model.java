package com.example.examplebackdropnandbottomsheet.ModelClasses;

import java.util.Date;

public class Activity_Model {

    String activity;
    String image;
    String start_date;
    String end_date;

    String object;

    public Activity_Model(String activity, String image, String start_date, String end_date,String object) {
        this.activity = activity;
        this.image = image;
        this.start_date = start_date;
        this.end_date = end_date;
        this.object=object;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
}
