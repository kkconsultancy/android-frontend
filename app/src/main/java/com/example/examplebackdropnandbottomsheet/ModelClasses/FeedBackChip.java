package com.example.examplebackdropnandbottomsheet.ModelClasses;

import com.google.gson.annotations.SerializedName;

public class FeedBackChip {
    @SerializedName("chip")
    String chipname;

    public String getChipname() {
        return chipname;
    }

    public void setChipname(String chipname) {
        this.chipname = chipname;
    }

    public FeedBackChip(String chipname) {
        this.chipname = chipname;
    }
}
