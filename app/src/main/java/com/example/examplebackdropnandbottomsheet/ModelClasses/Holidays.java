package com.example.examplebackdropnandbottomsheet.ModelClasses;

import com.google.gson.annotations.SerializedName;

public class Holidays {

    @SerializedName("to")
    private String to;
    @SerializedName("from")
    private String from;
    @SerializedName("message")
    private String HolidayMessage;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getHolidayMessage() {
        return HolidayMessage;
    }

    public void setHolidayMessage(String holidayMessage) {
        HolidayMessage = holidayMessage;
    }

    public Holidays(String to, String from, String holidayMessage) {
        this.to = to;
        this.from = from;
        HolidayMessage = holidayMessage;
    }
}
