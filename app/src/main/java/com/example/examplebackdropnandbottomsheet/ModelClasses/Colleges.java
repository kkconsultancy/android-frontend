package com.example.examplebackdropnandbottomsheet.ModelClasses;

import com.google.gson.annotations.SerializedName;

public class Colleges {
    @SerializedName("name")
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Colleges(String name) {
        this.name = name;
    }
}
