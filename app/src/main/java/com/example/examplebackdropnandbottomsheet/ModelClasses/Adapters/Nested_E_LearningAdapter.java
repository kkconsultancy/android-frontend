package com.example.examplebackdropnandbottomsheet.ModelClasses.Adapters;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.examplebackdropnandbottomsheet.ModelClasses.Activity_Model;
import com.example.examplebackdropnandbottomsheet.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.util.Date;
import java.util.List;

public class Nested_E_LearningAdapter extends RecyclerView.Adapter< Nested_E_LearningAdapter.MyViewHolder> {
    Date d =new Date();
    Context context;
    private List<Activity_Model> modelList;
    private final OnItemClick click;
    public  interface OnItemClick{
        void onItem(Activity_Model item);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {

       VideoView videoView ;
        ShimmerFrameLayout shimmerFrameLayout;

        public MyViewHolder(View view) {
            super(view);

            videoView=view.findViewById(R.id.e_learning_video);
            shimmerFrameLayout=view.findViewById(R.id.nested_recycle_shimmer);
        }
        public void bind(final Activity_Model l,final OnItemClick vv){
            videoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    vv.onItem(l);
                }
            });
        }
    }


    public  Nested_E_LearningAdapter(List<Activity_Model> moviesList, OnItemClick click) {
        this.modelList = moviesList;
        this.context=context;
        this.click=click;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_activity_recycleview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        //Uri uri=Uri.parse(modelList.get(position).getImage());

        holder.videoView.setVideoPath(modelList.get(position).getImage());

        holder.videoView.seekTo(100);
        //Picasso.get().load(modelList.get(position).getImage()).placeholder(R.drawable.ic_person_black_24dp).into(holder.videoView);
        holder.bind(modelList.get(position),click);


    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }
}
