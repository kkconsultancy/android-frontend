package com.example.examplebackdropnandbottomsheet.ModelClasses;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.example.examplebackdropnandbottomsheet.R;
import com.google.android.material.chip.Chip;

import java.util.List;

public class CollegeAdapter extends RecyclerView.Adapter<CollegeAdapter.MyViewHolder> {

private List<Colleges> moviesList;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public Chip college;

    public MyViewHolder(View view) {
        super(view);
        college = (Chip) view.findViewById(R.id.chipname);

    }
}


    public CollegeAdapter(List<Colleges> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chips_recycle_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.college.setText(moviesList.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
