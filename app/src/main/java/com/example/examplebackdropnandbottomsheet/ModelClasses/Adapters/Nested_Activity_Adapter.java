package com.example.examplebackdropnandbottomsheet.ModelClasses.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.examplebackdropnandbottomsheet.Activities.Student.View_Event;
import com.example.examplebackdropnandbottomsheet.ModelClasses.Activity_Model;
import com.example.examplebackdropnandbottomsheet.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Nested_Activity_Adapter extends RecyclerView.Adapter<Nested_Activity_Adapter.MyViewHolder> {
    Date d =new Date();
    Context context;
    private List<Activity_Model> modelList;
    private final OnItemClick click;
    public  interface OnItemClick{
        void onItem(Activity_Model item);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView activity_name;
        ImageView imageView;

        ShimmerFrameLayout shimmerFrameLayout;
        public RecyclerView nested_recycleview;
        public MyViewHolder(View view) {
            super(view);
            imageView= view.findViewById(R.id.imageActivity);
            shimmerFrameLayout=view.findViewById(R.id.nested_recycle_shimmer);
        }
        public void bind(final Activity_Model l,final OnItemClick vv){
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    vv.onItem(l);
                }
            });
        }
    }


    public Nested_Activity_Adapter(List<Activity_Model> moviesList,OnItemClick click) {
        this.modelList = moviesList;
        this.context=context;
        this.click=click;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_activity_recycleview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {



        try {
//            Date date1 = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy").parse(modelList.get(position).getEnd_date());
//
//            System.out.println("Date is ::::"+d.compareTo(date1));
//
//            if(d.compareTo(date1) > 0){
//
//
//            }

        }
        catch (Exception e){

        }
        Picasso.get().load(modelList.get(position).getImage())
                .placeholder(R.drawable.person_outline_black_24dp).into(holder.imageView);

        holder.bind(modelList.get(position),click);


    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }
}
