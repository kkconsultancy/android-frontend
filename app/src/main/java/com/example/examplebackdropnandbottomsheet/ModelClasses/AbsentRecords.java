package com.example.examplebackdropnandbottomsheet.ModelClasses;

import org.json.JSONArray;
import org.json.JSONObject;

public class AbsentRecords {

    String date;
    String subjects;
    String roll;

    public AbsentRecords(String date, String subjects, String roll) {
        this.date = date;
        this.subjects = subjects;
        this.roll = roll;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public String getRoll() {
        return roll;
    }

    public void setRoll(String roll) {
        this.roll = roll;
    }
}
