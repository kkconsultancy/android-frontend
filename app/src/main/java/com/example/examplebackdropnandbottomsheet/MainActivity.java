package com.example.examplebackdropnandbottomsheet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;


import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;



import com.example.examplebackdropnandbottomsheet.Academics.Attendence;
import com.example.examplebackdropnandbottomsheet.Academics.Complaints;
import com.example.examplebackdropnandbottomsheet.Academics.Student.Fragment_Marks;
import com.example.examplebackdropnandbottomsheet.Academics.Teacher.AttendenceTeacher;
import com.example.examplebackdropnandbottomsheet.Activities.Student.Activity_Student;
import com.example.examplebackdropnandbottomsheet.Administration.Student.Transport_Details;
import com.example.examplebackdropnandbottomsheet.MalbClasses.HttpHandler;
import com.example.examplebackdropnandbottomsheet.ViewPager.BottomPagerAdapter;


import com.example.examplebackdropnandbottomsheet.profile.Profile;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import com.google.firebase.auth.FirebaseAuth;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tyrantgit.explosionfield.ExplosionField;

import static com.example.examplebackdropnandbottomsheet.R.drawable.gradient;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomSheetBehavior behavior;
    CircularImageView profile;
    View bottomSheet_navigation;
    BottomPagerAdapter pagerAdapter;
    int c=0,d=0;
    ViewPager pager;
    BottomNavigationView navigation;
    ImageView menu;
    View bottomSheet;

    TextView logout,share,about,uid;

    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            getSupportActionBar().hide();
            sharedpreferences = getSharedPreferences("User", Context.MODE_PRIVATE);
        }
        catch (Exception e){

        }


        menufuctionalities();




        try {

            pager = (ViewPager) findViewById(R.id.container);
            pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
            pager.setOffscreenPageLimit(30);
            setUpPager(pager);


        }
        catch (Exception e){

        }


        try {

            navigation = findViewById(R.id.bottom_navigation);
            navigation.setSelectedItemId(R.id.academics);
            navigation.setOnNavigationItemSelectedListener(this);


        }
        catch (Exception e)
        {}


        try {
            bottomSheet = findViewById(R.id.bottomsheetmainpage);
            menu = findViewById(R.id.menu);
            menu.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("ResourceAsColor")
                @Override
                public void onClick(View v) {
                    try {
                        if (d == 0) {
                            d = d + 1;
                            showfeedback();
                            slideDown(bottomSheet);
                        } else {
                            slideUp(bottomSheet);
                            d = 0;
                        }
                    } catch (Exception e) {

                    }
                }
            });
        }
        catch (Exception e){

        }
    }

    public void menufuctionalities(){


        try{
            uid=findViewById(R.id.uid);
            uid.setText(sharedpreferences.getString("uid","").toUpperCase());
        }
        catch (Exception e){

        }
        try{

            profile=findViewById(R.id.profile);
            Picasso.get().load("https://firebasestorage.googleapis.com/v0/b/redants4site.appspot.com/o/Photos%2F16K61A0554.JPG?alt=media&token=79b3ef5c-39a5-4b51-a54b-3f96d9102de2").placeholder(R.drawable.sasilogo).into(profile);

            profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pager.setCurrentItem(1);
                    slideUp(bottomSheet);

                }
            });

        }
        catch (Exception e){
        }


        try{

//            logout=findViewById(R.id.logout);
//            logout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    FirebaseAuth.getInstance().signOut();
//
//
//                    SharedPreferences.Editor editor = sharedpreferences.edit();
//                    editor.putString("uid", "");
//                    editor.apply();
//
//                    Intent i=new Intent(getApplicationContext(),LogoActivity.class);
//                    startActivity(i);
//                    finish();
//                }
//            });



            try{

                TextView complaints=findViewById(R.id.complaints);
                complaints.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        View view2=getCurrentFocus();
                        slideUp(view2);
                        pager.setCurrentItem(2);
                    }
                });

            }
            catch (Exception e){

            }

        }
        catch (Exception e){ }
    }

    public void slideUp(final View view){


        menu.setImageDrawable(getResources().getDrawable(R.drawable.menu_black_24dp));
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationY",0f);
        animation.setDuration(700);

        animation.start();

    }

    public void slideDown(View view){

        menu.setImageDrawable(getResources().getDrawable(R.drawable.close_black_24dp));
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationY",   2000f);
        animation.setDuration(1000);
        animation.start();
    }

    private  void setUpPager(ViewPager pager){
        try {
            pagerAdapter = new BottomPagerAdapter(getSupportFragmentManager());

            pagerAdapter.addFrag(new Attendence());
            pagerAdapter.addFrag(new Profile());
            pagerAdapter.addFrag(new Complaints());
            pagerAdapter.addFrag(new AttendenceTeacher());
            pagerAdapter.addFrag(new Fragment_Marks());
            pagerAdapter.addFrag(new Activity_Student());
            pagerAdapter.addFrag(new Transport_Details());
            pager.setAdapter(pagerAdapter);

        }
        catch (Exception e){

        }

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {



            switch (position){
                case 0:

                    return new Attendence();
                case 1:

                    return new Profile();
                case 2:
                    return new Complaints();
                case 3:
                    return new AttendenceTeacher();
                case 4:
                    return new Fragment_Marks();

                case 5:
                    return new Activity_Student();
                case 6:
                    return new Transport_Details();

                default:
                    return new Profile();
            }

        }

        @Override
        public int getCount() {
            return 8;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.academics:
                 //fragment=new Profile();

                //slideDown(bottomSheet);

                pager.setCurrentItem(0);
                bottomsheetNavigationTranslations(1);
                break;
            case R.id.administration:
//                slideDown(bottomSheet);
                pager.setCurrentItem(6);
                bottomsheetNavigationTranslations(2);
                break;
            case R.id.activities:
                //slideDown(bottomSheet);
                pager.setCurrentItem(5);
                break;


        }
        return true;
    }

    void bottomsheetNavigationTranslations(int page){

        //pager.setCurrentItem(6);

        bottomSheet_navigation = findViewById(R.id.academics_bottomsheet);
        behavior = BottomSheetBehavior.from(bottomSheet_navigation);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomsheet, int newState) {
//
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//                // React to dragging events
//
//
//            }
//        });

        switch (page){
            case 1:
                Map<Integer,String> academics=new HashMap<>();
                academics.put(0,"Attendance");
                academics.put(4,"External Marks");
                TableLayout t1 =(TableLayout) findViewById(R.id.logousertable);
                try {
                    t1.removeAllViewsInLayout();
                }
                catch (Exception e){

                }
                Collection<String> values = academics.values();
                for (final Map.Entry<Integer, String> entry : academics.entrySet()) {

                    TableRow tr = new TableRow(getApplicationContext());


                    final TextView tk = new TextView(getApplicationContext());
                    tk.setText(entry.getValue());
                    tk.setGravity(Gravity.CENTER);

                    tk.setTextAppearance(this, R.style.dynamictextStyle);
                    tk.setTextColor(getResources().getColor(R.color.black));
                    tk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pager.setCurrentItem(entry.getKey());
                            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }
                    });
                    tr.setPadding(5,12,5,12);
                    tr.addView(tk);

                   t1.addView(tr, new TableLayout.LayoutParams(
                            TableLayout.LayoutParams.WRAP_CONTENT,
                            TableLayout.LayoutParams.WRAP_CONTENT));
                }


                break;
            case 2:
                final Map<Integer,String> administration=new HashMap<>();
                administration.put(6,"Fee Details");

                TableLayout t2 =(TableLayout) findViewById(R.id.logousertable);
                try {
                    t2.removeAllViewsInLayout();
                }
                catch (Exception e){

                }

                for (final Map.Entry<Integer, String> entry : administration.entrySet()) {
                    TableRow tr = new TableRow(getApplicationContext());


                    final TextView tk = new TextView(getApplicationContext());

                    tk.setText(entry.getValue());

                    tk.setTextAppearance(this, R.style.dynamictextStyle);
                    tk.setGravity(Gravity.CENTER);
                    tr.addView(tk);
                    tk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pager.setCurrentItem(entry.getKey());
                            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }
                    });
                    tk.setTextColor(getResources().getColor(R.color.black));
                    tr.setPadding(5,12,5,12);

                    t2.addView(tr, new TableLayout.LayoutParams(
                            TableLayout.LayoutParams.WRAP_CONTENT,
                            TableLayout.LayoutParams.WRAP_CONTENT));
                }

                break;

        }
    }

    void showfeedback(){



        slideDown(bottomSheet);

//        View bottomSheet_feedback= findViewById(R.id.feedback);
//        BottomSheetBehavior behavior1;
//        behavior1 = BottomSheetBehavior.from(bottomSheet_feedback);
//        behavior1.setState(BottomSheetBehavior.STATE_EXPANDED);
//        behavior1.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//
//
//
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//                // React to dragging events
//            }
//        });
    }


}






//                    TranslateAnimation anim = new TranslateAnimation(0, 0, yUP, amountToMoveDown);
//                    anim.setDuration(1000);
//
//                    anim.setAnimationListener(new TranslateAnimation.AnimationListener() {
//
//                        @Override
//                        public void onAnimationStart(Animation animation) {
//                        }
//
//                        @Override
//                        public void onAnimationRepeat(Animation animation) {
//                        }
//
//                        @Override
//                        public void onAnimationEnd(Animation animation) {
//                            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) bottomSheet.getLayoutParams();
//                            params.topMargin += amountToMoveDown;
//                            params.leftMargin += 0;
//                            bottomSheet.setLayoutParams(params);
//                        }
//                    });
//
//                    bottomSheet.startAnimation(anim);