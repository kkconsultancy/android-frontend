package com.example.examplebackdropnandbottomsheet;


import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpConnectionGetData extends AsyncTask<Void, Void, String> {
    LogoActivity previos;
    String q,urlDaa,method;
    String responseD;
    int key;


    HttpConnectionGetData(LogoActivity instance, String urlDaa, String method, String query,int key){
        previos =instance;
        this.q=query;
        this.urlDaa=urlDaa;
        this.method=method;
        this.key=key;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        responseD="";
    }

    @Override
    protected String doInBackground(Void... params) {
        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        // Will contain the raw JSON response as a string.
        String forecastJsonStr = "";

        try {

            URL url = new URL(urlDaa);
            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod(method);
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");

            OutputStream os = urlConnection.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(q);

            osw.flush();
            osw.close();
            os.close();

            urlConnection.connect();
            int lengthOfFile = urlConnection.getContentLength();
            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                return null;
            }
            forecastJsonStr = buffer.toString();
            Log.e("Json1", forecastJsonStr);

            responseD=forecastJsonStr;



            return forecastJsonStr;
        } catch (IOException e) {
            Log.e("PlaceholderFragment", "Error ", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }
        }
        return forecastJsonStr;
    }

    public String getResponse(){
        return responseD;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);


            previos.callback(key);


    }


}